<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*

 *  @author     : Y Nagesh,SVSK TEAM

 *  date        : 10 july, 2020

 *  Human Resource Management System

 */

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        /*cache control*/
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    }

    //Default function, redirects to logged in user area
    public function index() {

        if ($this->session->userdata('admin_login') == 1)
            redirect(site_url('admin/dashboard'), 'refresh');
        if ($this->session->userdata('employee_login') == 1)
            redirect(site_url('employee/dashboard'), 'refresh');

        $this->load->view('backend/login');

    }


    //Validating login informations from ajax request
    function validate_login() {
        $email      =   $this->input->post('email');
        $password   =   $this->input->post('password');
        $credential	=	array(	'email' => $email , 'password' => sha1($password) );

        // Checking login credential for users of the system
        $query = $this->db->get_where('user' , $credential);
        if ($query->num_rows() > 0) {
            $row = $query->row();

            //setting the session parameters for admin
            if ($row->type == 1) {
                $this->session->set_userdata('admin_login' , '1');
                $this->session->set_userdata('login_type' , 'admin');
            }

            //setting the session parameters for employees
            if ($row->type == 2) {
                $this->session->set_userdata('employee_login' , '1');
                $this->session->set_userdata('login_type' , 'employee');
            }
            //setting the common session parameters for all type of users of the system
            $this->session->set_userdata('login_user_id' , $row->user_id);
            
            $this->db->where('user_id', $row->user_id);
            $this->db->update('user', array('login_status'=>1));

            $queryLType = $this->db->get_where('login_type',array('status'=>1))->result_array();
            foreach ($queryLType as $typeArray) {
                $type = $typeArray['type'];
            }


            if($type == 1)
            {
                $attn_data['attendance_code']   = substr(md5(rand(100000000, 20000000000)), 0, 7);

               $attn_data['user_id']           = $row->user_id;

                $attn_data['date']              = strtotime(date("d-m-Y"));

                $attn_data['status']            = 1;

                 $getAtt = $this->db->get_where('attendance',array('date' => $attn_data['date'],'user_id'=>$row->user_id))->result_array();
                 //echo "rows". $query->num_rows();
                 foreach($getAtt as $typeArr)
                 {
                     $id = $typeArr["user_id"];
                 }
                 if($id < 1)
                 {
                    $this->db->insert('attendance', $attn_data);
                    //echo "succ";
                 }
                // die;
            }

            $userId = $row->user_id;
            $conn = @mysqli_connect('localhost','svsktbgs_demohrm','demohrms','svsktbgs_demohrms');
                // if($conn)
                // {
                date_default_timezone_set("Asia/Kolkata"); 
                   //echo "success"; 
                   $ip = getenv('REMOTE_ADDR');
                    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
                    ;
                    $token_id = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
                    $city = $geo["geoplugin_city"];
                    $latitude=$geo["geoplugin_latitude"];
                    $longitude=$geo["geoplugin_longitude"];
                    $region = $geo["geoplugin_regionName"];
                    $date = date('Y-m-d H:i:s',time());
                    $onlydate = date('Y-m-d',time());
              $sql = "insert into user_log(user_id,ip,city,region,lattitude,longitude,created_at,date,token,login_status,live_status,status)values($userId,'".$ip."','".$city."','".$region."','".$latitude."','".$longitude."','".$date."','".$onlydate."','".$token_id."',1,1,1)";
                    $result = @mysqli_query($conn,$sql);
                    
                    // if($result)
                    // {
                    //     echo "Login IP : ". $ip;
                        
                    // }
                    // else
                    // {
                    //     echo "IP failed";
                    // }
                 
                // }
                // else
                // {
                //     echo "failed";
                // }
               
                $this->session->set_userdata('token' , $token_id);
                 //die;
            redirect(site_url($this->session->userdata('login_type').'/dashboard'), 'refresh');
        } else {
            $this->session->set_flashdata('login_error', 'Invalid Login');
            redirect(site_url('login'), 'refresh');
        }
    }

    function forgot_password() {
        $this->load->view('backend/forgot_password');
    }

    function reset_password() {
        $email = $this->input->post('email');
        $query = $this->db->get_where('user', array('email' => $email));
        if ($query->num_rows() > 0) {
            // create a new password
            $new_password   =   substr( md5( rand(100000000,20000000000) ) , 0,7);
            // update the password in database
            $this->db->where('email', $email);
            $this->db->update('user', array('password' => sha1($new_password)));
            // send an email with the new password
            $this->email_model->password_reset_email($new_password, $email);
            $this->session->set_flashdata('reset_success', get_phrase('check_your_email_for_new_password'));
            redirect(site_url('login/forgot_password'),'refresh');

        } else {
            $this->session->set_flashdata('reset_error', get_phrase('failed_to_reset_password'));
            redirect(site_url('login/forgot_password'),'refresh');
        }
    }


    // logout
    function logout()
    {
        $this->session->sess_destroy();
          $userId = $this->session->userdata('login_user_id');
          $userType = $this->session->userdata('login_type');
          $token = $this->session->userdata('token');
         $this->db->where('token', $token);
         date_default_timezone_set("Asia/Kolkata"); 
         $date = date('Y-m-d H:i:s',time());
         $time = date('H:i:s',time());
        $onlydate = date('Y-m-d',time());
        $this->db->update('user_log', array('logout_date_time' => $date,'logout_date'=>$onlydate,'logout_time'=>$time,'login_status'=>2,'live_status'=>0));
        
        $this->db->where('user_id',$userId);
        $this->db->update('user', array('login_status'=>0));
       
        redirect(site_url('login') , 'refresh');
    }

}
