
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class="entypo-plus-circled"></i>
                    <?php echo get_phrase('add_project'); ?>
                </div>
            </div>

            <div class="panel-body">

                <?php echo form_open(site_url('admin/projects/create'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('project_name'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="project_name" required value="" autofocus />
                    </div>
                
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('client'); ?></label>

                    <div class="col-sm-3">
                        <select class="form-control" name="client" required >
                            <option value = "">Select</option>
                            <option value = "own">Own</option>
							<option value = "others">Others</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('start_date'); ?></label>

                    <div class="col-sm-3">
                        <input type="date" class="form-control  datetimepicker" name="start_date" required value="" />
                    </div>
                
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('end_date'); ?></label>

                    <div class="col-sm-3">
                        <input type="date" class="form-control  datetimepicker" name="end_date" required value="" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('priority'); ?></label>

                    <div class="col-sm-3">
                        <select name="priority" class="form-control" required>
                             <option value = "high"><?php echo get_phrase('select'); ?></option>
                            <option value = "high"><?php echo get_phrase('high'); ?></option>
                             <option value = "medium"><?php echo get_phrase('medium'); ?></option>
                             <option value = "low"><?php echo get_phrase('low'); ?></option>
                        </select>
                    </div>
            
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('project_leader'); ?></label>

                    <div class="col-sm-3">
                        <select name="project_leader" class="form-control" required>
                            <?php
                                $projectLeader = $this->db->get_where('user',array('type'=>2))->result_array();
                                foreach($projectLeader as $projectLeaderName)
                                {
                            ?>
                            <option value = "<?php echo $projectLeaderName['name']; ?>"><?php echo $projectLeaderName['name']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('team'); ?></label>

                    <div class="col-sm-5">
                        <select name="team[]" class="selectpicker form-control" multiple  required data-max-options="2" data-live-search="true">
                              <?php
                                $team = $this->db->get_where('user',array('type'=>2))->result_array();
                                foreach($team as $teammembers)
                                {
                            ?>
                            <option value = "<?php echo $teammembers['user_code']; ?>"><?php echo $teammembers['name']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                    </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                    <div class="col-sm-5">
                        <textarea rows="4" class="form-control summernote" name = "description" placeholder="Enter your description here"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label"><?php echo get_phrase('upload_document'); ?></label>

                    <div class="col-sm-5">
                        <input class="form-control upload1" name = "upload_document" type="file">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('create'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); 
                 ?>
            </div>
        </div>
    </div>
</div>
<!-- Datetimepicker JS -->
		<script src="https://dreamguys.co.in/smarthr/orange/assets/js/moment.min.js"></script>
		<script src="https://dreamguys.co.in/smarthr/orange/assets/js/bootstrap-datetimepicker.min.js"></script>
		
		<!-- Summernote JS -->
		<script src="https://dreamguys.co.in/smarthr/orange/assets/plugins/summernote/dist/summernote-bs4.min.js"></script>

		<!-- Custom JS -->
		<script src="https://dreamguys.co.in/smarthr/orange/assets/js/app.js"></script>
<script type="text/javascript">

    $( document ).ready(function() {

        // SelectBoxIt Dropdown replacement
        if($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function(i, el)
            {
                var $this = $(el),
                    opts = {
                        showFirstOption: attrDefault($this, 'first-option', true),
                        'native': attrDefault($this, 'native', false),
                        defaultText: attrDefault($this, 'text', ''),
                    };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>
<script>
			$(function() {  
			 $('#multiselect').multiselect();
			});
			</script>
