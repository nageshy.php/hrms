<?php
$edit_data = $this->db->get_where('expense')->result_array();
foreach($edit_data as $row) { ?>
<?php } ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('used_expense'); ?>
                    </div>
                </div>

                <div class="panel-body">

                    <?php echo form_open(site_url('admin/expense/used/'). $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('expense_title'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="title" required value="<?php echo $row['title']; ?>" autofocus />
                             <input type="hidden" class="form-control" name="expense_code" value="<?php echo $row['expense_code']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                        <div class="col-sm-8">
                            <textarea class="form-control" name="description" rows="3" required><?php echo $row['description']; ?></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('amount'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="balance_amount" required value="<?php echo $row['balance_amount']; ?>" readonly/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('amount_used'); ?></label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="amount_used" required value="<?php echo $row['amount_used']; ?>" />
                        </div>
                    </div>
                    
                    
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('bill/invoice_availabe'); ?></label>

                    <div class="col-sm-5">
                        <input type="radio" name="bill" required value="yes" /> Yes
                        <input type="radio"  name="bill" required value="no" /> No
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('receive_from'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="receive_from" required value="<?php echo $row['receive_from']; ?>" />
                    </div>
                </div>
                
                 <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('mode_of_amount'); ?></label>

                    <div class="col-sm-5">
                        <select class="form-control" name="mode_of_amount" required value="">
                            <option value = "<?php echo $row['mode_of_amount']; ?>" selected="true"><?php echo $row['mode_of_amount']; ?></option>
                            <option value = "cash"><?php echo get_phrase('cash'); ?></option>
                            <option value = "AFT"><?php echo get_phrase('AFT'); ?></option>
                            <option value = "cheque"><?php echo get_phrase('cheque'); ?></option>
                        </select>
                    </div>
                </div>
                

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('date'); ?></label>

                        <div class="col-sm-5">
                            <input type="date" class="form-control" name="date" value="<?php echo date('Y-m-d', $row['date']); ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info"><?php echo get_phrase('used'); ?></button>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>

