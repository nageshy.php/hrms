<?php
$edit_data = $this->db->get_where('projects', array('id' => $param2))->result_array();
foreach($edit_data as $row) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <i class="entypo-plus-circled"></i>
                        <?php echo get_phrase('edit_projects'); ?>
                    </div>
                </div>

                <div class="panel-body">

                    <?php echo form_open(site_url('admin/projects/update/'). $param2, array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

                    <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('project_name'); ?></label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control" name="project_name" required value="<?php echo $row['project_name'];?>" autofocus />
                        <input type="hidden" class="form-control" name="project_code" required value="<?php echo $row['project_code'];?>" autofocus />
                    </div>
                
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('client'); ?></label>

                    <div class="col-sm-3">
                        <select class="form-control" name="client" required >
                            <option value = "<?php echo $row['project_name'];?>"><?php echo $row['client'];?></option>
                            <option value = "">Select</option>
                            <option value = "own">Own</option>
							<option value = "others">Others</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('start_date'); ?></label>

                    <div class="col-sm-3">
                        <input type="date" class="form-control  datetimepicker" name="start_date" required value="<?php echo date('yy-m-d',$row['start_date']);?>" />
                    </div>
                
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('end_date'); ?></label>

                    <div class="col-sm-3">
                        <input type="date" class="form-control  datetimepicker" name="end_date" required value="<?php echo date('yy-m-d',$row['end_date']);?>" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('priority'); ?></label>

                    <div class="col-sm-3">
                        <select name="priority" class="form-control" required>
                            <option value = "<?php echo $row['priority'];?>"><?php echo $row['priority'];?></option>
                             <option value = "high"><?php echo get_phrase('select'); ?></option>
                            <option value = "high"><?php echo get_phrase('high'); ?></option>
                             <option value = "medium"><?php echo get_phrase('medium'); ?></option>
                             <option value = "low"><?php echo get_phrase('low'); ?></option>
                        </select>
                    </div>
            
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('project_leader'); ?></label>

                    <div class="col-sm-3">
                        <select name="project_leader" class="form-control" required>
                            <option value = "<?php echo $row['project_leader'];?>" ><?php echo $row['project_leader'];?></option>
                            <?php
                                $projectLeader = $this->db->get_where('user',array('type'=>2))->result_array();
                                foreach($projectLeader as $projectLeaderName)
                                {
                            ?>
                            <option value = "<?php echo $projectLeaderName['name']; ?>"><?php echo $projectLeaderName['name']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-ta" class="col-sm-3 control-label"><?php echo get_phrase('team'); ?></label>

                    <div class="col-sm-5">
                        <select name="team[]" class="selectpicker form-control" multiple  required data-max-options="2" data-live-search="true">
                              <?php
                                $team = $this->db->get_where('user',array('type'=>2))->result_array();
                                foreach($team as $teammembers)
                                {
                            ?>
                            <option value = "<?php echo $teammembers['user_code']; ?>"><?php echo $teammembers['name']; ?></option>
                             <?php } ?>
                        </select>
                    </div>
                    </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                    <div class="col-sm-5">
                        <textarea rows="4" class="form-control summernote" name = "description" placeholder="Enter your description here"><?php echo $row['description'];?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-5 control-label"><?php echo get_phrase('upload_document'); ?></label>

                    <div class="col-sm-5">
                        <input class="form-control upload1"value = "<?php echo $row['document'];?>" name = "upload_document1" type="HIDDEN">
                        <input class="form-control upload1"value = "<?php echo $row['document'];?>" name = "upload_document" type="file">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('update'); ?></button>
                    </div>
                </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script type="text/javascript">

    $( document ).ready(function() {

        // SelectBoxIt Dropdown replacement
        if($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function(i, el)
            {
                var $this = $(el),
                    opts = {
                        showFirstOption: attrDefault($this, 'first-option', true),
                        'native': attrDefault($this, 'native', false),
                        defaultText: attrDefault($this, 'text', ''),
                    };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>
