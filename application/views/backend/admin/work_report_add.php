<div class="modal-content" style="background-image:url(https://c1.wallpaperflare.com/preview/397/809/215/arrows-box-business-chalk.jpg)">
                
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="color: white !important;">Work Report</h4>
                </div>
                
                <div class="modal-body" style="height:500px; overflow:auto; width: 100%;">
               
                    <form role="form" method="POST" action="">
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="project">Projects</label>
                                    <select class="form-control" id="project" >
                                      <option>Sirisam</option>
                                      <option>Rko</option>
                                      
                                    </select>                          
                        </div>
                            </div>
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="modules">Project Modules</label>
                                <select class="form-control" id="modules">
                                  <option>Services</option>
                                  <option>Products</option>                               
                                  
                                </select>
                    </div>
                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="url types">Url Types</label>
                                    <select class="form-control" id="url types" >
                                      <option>General</option>
                                      <option>Fully Qualified </option>
                                      <option>From the Root </option>
                                     <option> Relative </option>

                                      
                                    </select>                          
                        </div>
                            </div>
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="types">Work Types</label>
                                <select class="form-control" id="types">
                                  <option>General</option>
                                  <option>Full time</option>   
                                  <option value="">Part Time</option>                            
                                  
                                </select>
                    </div>
                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="work">Work From</label>
                                    <select class="form-control" id="work" >
                                      <option>Work From Home</option>
                                      <option>Work From Office</option>
                                      
                                    </select>                          
                        </div>
                            </div>
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="workdate">Work Date</label>
                                <input type="date" id="workdate" class="form-control">
                    </div>
                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="duration">Duration(HH:MM)</label><br>
                                    <select class="form-control-1" id="work">
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                        <option>06</option>
                                        <option>07</option>
                                        <option>08</option>
                                        <option>09</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                        
                                      </select>      
                                      <select class="form-control-1" id="work" >
                                          <option>00</option>
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                        <option>06</option>
                                        <option>07</option>
                                        <option>08</option>
                                        <option>09</option>
                                        <option>10</option>
                                        <option>11</option>
                                        <option>12</option>
                                        <option>13</option>
                                        <option>14</option>
                                        <option>15</option>
                                        <option>16</option>
                                        <option>17</option>
                                        <option>18</option>
                                        <option>19</option>
                                        <option>20</option>
                                        <option>21</option>
                                        <option>22</option>
                                        <option>23</option>
                                        <option>24</option>
                                        <option>25</option>
                                        <option>26</option>
                                        <option>27</option>
                                        <option>28</option>
                                        <option>29</option>
                                        <option>30</option>
                                        <option>31</option>
                                        <option>32</option>
                                        <option>33</option>
                                        <option>34</option>
                                        <option>35</option>
                                        <option>36</option>
                                        <option>37</option>
                                        <option>38</option>
                                        <option>39</option>
                                        <option>40</option>
                                        <option>41</option>
                                        <option>42</option>
                                        <option>43</option>
                                        <option>44</option>
                                        <option>45</option>
                                        <option>46</option>
                                        <option>47</option>
                                        <option>48</option>
                                        <option>49</option>
                                        <option>50</option>
                                        <option>51</option>
                                        <option>52</option>
                                        <option>53</option>
                                        <option>54</option>
                                        <option>55</option>
                                        <option>56</option>
                                        <option>57</option>
                                        <option>58</option>
                                        <option>59</option>

                                        
                                      </select>                  
                        </div>
                            </div>
                      
                      
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">  
                                
                                <div class="form-group">
                                    <label for="Details">Details</label>
                                  <textarea name="" id="Details" cols="" rows="5" class="form-control" placeholder="Details"></textarea>                      
                        </div>
                            </div>
                      
                      
                        </div>



 


                      
                    </form>
                    
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div