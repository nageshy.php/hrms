  <style type="text/css">
      .salary-slip {
   margin: 15px;
}
 .salary-slip .empDetail {
   width: 100%;
   text-align: left;
   border: 1px solid black;
   border-collapse: collapse;
   table-layout: fixed;
}
.salary-slip .empDetail1{
  width: 50%;
  text-align: left;
  border:1px solid black;
  border-collapse:collapse;
  table-layout: fixed;
  float:left;
}
 .salary-slip .head {
   margin: 10px;
   margin-bottom: 50px;
   width: 100%;
}
}
 .salary-slip .companyName {
   text-align:center;
   font-size: 25px;
   font-weight: bold;
}
 .salary-slip .salaryMonth {
   text-align: center;
}
 .salary-slip .table-border-bottom {
   border-bottom: 1px solid;
}
 .salary-slip .table-border-right {
   border-right: 1px solid;
}
 .salary-slip .myBackground {
   padding-top: 10px;
   text-align: left;
   border: 1px solid black;
   height: 40px;
}
 .salary-slip .myAlign {
   text-align: center;
   border-right: 1px solid black;
}
 .salary-slip .myTotalBackground {
   padding-top: 10px;
   text-align: left;
   background-color: #ebf1de;
   border-spacing: 0px;
}
 .salary-slip .align-4 {
   width: 25%;
   float: left;
}
 .salary-slip .tail {
   margin-top: 35px;
}
 .salary-slip .align-2 {
   margin-top: 25px;
   width: 50%;
   float: left;
}
 .salary-slip .border-center {
   text-align: center;
}
 .salary-slip .border-center th, .salary-slip .border-center td {
   border: 1px solid black;
}
 .salary-slip th, .salary-slip td {
      padding-left: 6px;
    vertical-align: inherit!important;
    border: 1px solid black
}
 

 body{
        font-family: monospace;
    }
    </style>

<?php
$edit_data = $this->db->get_where('payroll', array('payroll_id' => $param2))->result_array();

foreach ($edit_data as $row):
    $user = $this->db->get_where('user', array('user_id' => $row['user_id']))->row();
     $desig = $this->db->get_where('designation', array('department_id' => $user->department_id))->row();
     $bank = $this->db->get_where('bank', array('bank_id' => $user->bank_id))->row();
   

    $date = explode(',', $row['date']);
    //print_r($date);
        $month_list = array('january', 'february', 'march', 'april', 'may', 'june', 'july',
        'august', 'september', 'october', 'november', 'december');
    for ($i = 1; $i <= 12; $i++)
        if ($i == $date[0])
            $month = get_phrase($month_list[$i]);

        $time_input = strtotime($row['date']);  
        $date_input = getDate($time_input);
         //$date1 = getDate();
         //print_r($date_input);
         
    ?>
    <div id="payroll_print" style = "display: none;">
        <table width="100%" border="0">
            <tr>
                <td width="50%"><img src="<?php echo base_url();?>uploads/logo.png" style="max-height:80px;"></td>
                <td align="right">
                    <h4><?php echo get_phrase('payslip_code'); ?> : <?php echo $row['payroll_code']; ?></h4>
                    <h5><?php echo get_phrase('employee'); ?> : <?php echo $user->name; ?></h5>
                    <h5><?php echo get_phrase('date'); ?> : <?php echo $month . ', ' . $date[1]; ?></h5>
                    <h5>
                        <?php echo get_phrase('status'); ?> :
                        <?php
                        if($row['status'] == 0)
                            echo get_phrase('unpaid');
                        else
                            echo get_phrase('paid'); ?>
                    </h5>
                </td>
            </tr>
        </table>
        
        <hr><br>
        <h4 style="text-align: center;"><?php echo get_phrase('allowance_summary'); ?></h4>
        <?php if($row['allowances'] == '') { ?>
            <div class="alert alert-info"><?php echo get_phrase('no_allowances');?></div>
        <?php } else { ?>
            <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th width="60%"><?php echo get_phrase('type'); ?></th>
                        <th><?php echo get_phrase('amount'); ?></th>
                    </tr>
                </thead>

                <tbody>
                    <div>
                        <?php
                        $total_allowance    = 0;
                        $allowances         = json_decode($row['allowances']);
                        $i = 1;
                        foreach ($allowances as $allowance)
                        {
                            $total_allowance += $allowance->amount; ?>
                            <tr>
                                <td class="text-center"><?php echo $i++; ?></td>
                                <td>
                                    <?php echo $allowance->type; ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $allowance->amount; ?>
                                </td>
                            </tr>
                        <?php }  

                        ?>
                    </div>
                </tbody>
            </table>
        <?php } ?>
        
        <br>
        <h4 style="text-align: center;"><?php echo get_phrase('deduction_summary'); ?></h4>
        <?php if ($row['deductions'] == '') { ?>
            <div class="alert alert-info"><?php echo get_phrase('no_deductions'); ?></div>
        <?php } else { ?>
            <table class="table table-bordered" width="100%" border="1" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th width="60%"><?php echo get_phrase('type'); ?></th>
                        <th><?php echo get_phrase('amount'); ?></th>
                    </tr>
                </thead>

                <tbody>
                <div>
                    <?php
                    $total_deduction = 0;
                    $deductions = json_decode($row['deductions']);
                    $i = 1;
                    foreach ($deductions as $deduction) {
                        $total_deduction += $deduction->amount;
                        ?>
                        <tr>
                            <td class="text-center"><?php echo $i++; ?></td>
                            <td>
                                <?php echo $deduction->type; ?>
                            </td>
                            <td class="text-right">
                                <?php echo $deduction->amount; ?>
                            </td>
                        </tr>
                    <?php } ?>
                </div>
                </tbody>
            </table>
        <?php } ?>
        
        <br>
        <h3 style="text-align: center; margin-bottom: 0px;"><?php echo get_phrase('payslip_summary'); ?></h3>
        <center><hr style="margin: 5px 0px 5px 0px; width: 50%;"></center>
        <center>
            <table>
                <tr>
                    <td style="font-weight: 600; font-size: 15px; color: #000;">
                        <?php echo get_phrase('basic_salary'); ?></td>
                    <td style="font-weight: 600; font-size: 15px; color: #000; width: 15%;
                        text-align: center;"> : </td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        text-align: right;"><?php echo $user->joining_salary; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: 600; font-size: 15px; color: #000;">
                        <?php echo get_phrase('total_allowance'); ?></td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        width: 15%; text-align: center;"> : </td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        text-align: right;"><?php echo $total_allowance; ?></td>
                </tr>
                <tr>
                    <td style="font-weight: 600; font-size: 15px; color: #000;">
                        <?php echo get_phrase('total_deduction'); ?></td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        width: 15%; text-align: center;"> : </td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        text-align: right;"><?php echo $total_deduction; ?></td>
                </tr>
                <tr>
                    <td colspan="3"><hr style="margin: 5px 0px;"></td>
                </tr>
                <tr>
                    <td style="font-weight: 600; font-size: 15px; color: #000;">
                        <?php echo get_phrase('net_salary'); ?></td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        width: 15%; text-align: center;"> : </td>
                    <td style="font-weight: 600; font-size: 15px; color: #000;
                        text-align: right;">
                        <?php
                        $net_salary = $user->joining_salary + $total_allowance - $total_deduction;
                        echo $net_salary; ?>
                    </td>
                </tr>
            </table>
        </center>
        <br>
    </div>

    <!-- <a onClick="PrintElem('#payroll_print')" class="btn btn-primary btn-icon icon-left hidden-print">
        <?php //echo get_phrase('print_payslip_details'); ?>
        <i class="entypo-doc-text"></i>
    </a> -->


<?php endforeach; 
 $attendance = $this->db->get_where('attendance',array('user_id' => '2','status'=>'1'))->row();
 //$this->db->group_by(array("date"));
     //echo $row['user_id'] . "<br/>";
    //echo $attendance->date;
        // echo  $row['user_id']. "<br/>";
                        $c=1;
                        foreach ($attendance as $attendVal) {
                           //echo $attendance->attendance_id;
                           $userId = $attendVal->user_id;
                           $dateA = $attendVal->date;
                           echo $userId ." ". date('m-y',$dateA) . "<br/>";
                           $c++;
                        }
                        echo $c/2-0.5;

?>

<div class="salary-slip" >
            <table class="empDetail">
               <tr style='background-color: #c2d69b'>
                <td colspan='8' class="companyName"  style="text-align: center;">
               <!-- <h4> SALARY SLIP FOR THE MONTH OF AUGUST 2020</h4> -->
                <img src=" <?php echo base_url();?>uploads/logo.png" width="150px">
               <p>Plot no 81/10, 3rd Floor, Street No 1 Patrikanagar,
Madhapur, Hyderabad, Telangana 500081</p>
             </td>
              </tr>
              <tr style='background-color: #c2d69b'>
                <td colspan='8' class="companyName"  style="text-align: center;">
               <b> SALARY SLIP FOR THE MONTH OF 
                
                <?php //echo $month . ', ' . $date[1];
                //echo date('m-d', strtotime('last month'))
                $prevmonth = date("M Y",mktime(0,0,0,date("m", strtotime($month))-1,1,date("Y", strtotime($month))));
                echo $prevmonth;
                    ?></b></td>
              </tr>
              <tr><th><td></td></th></tr>
              <tr>
       <th>NAME </th>
        <td colspan="2"><?php echo $user->name; ?></td>
        
        <th>BANK NAME</th>
        <td colspan="2"><?php echo $bank->name; ?></td>
      
        <th>PAN NO</th>
        <td></td>
        </tr>
        <tr>
         <th>EMPLOYEE ID </th>
        <td colspan="2"><?php echo $user->user_code; ?></td>
        
        <th>BANK ACCOUNT NO</th>
        <td colspan="2"><?php echo $bank->account_number; ?></td>
       
        <th>WORK DAYS</th>
        <td></td>
         </tr>
        <tr>
        <th>DATE OF JOINING</th>
        <td colspan="2"><?php echo date('m/d/Y', $user->date_of_joining); ?></td>
      
       <th>Branch</th>
      <td colspan="2"><?php echo $bank->branch; ?></td>
      
       <th>Total Days</th>
      <td></td>
      </tr>
      <tr>
      <th>Designation</th>
      <td colspan="2"><?php echo $desig->name; ?></td>
      
      <th>EPF No</th>
      <td colspan="2"></td>
      
      <th>Lop</th>
      <td></td>
      </tr>
      <tr>
      <th>Location</th>
      <td colspan="2"><?php echo $user->local_address; ?></td>
     
      <th>ESI NO</th>
      <td colspan="2"></td>
      
      <th>Arrear Days</th>
      <td></td>
      </tr>
      <tr>
        <th></th>
        <td></td>
      </tr>
    </table>
    <?php if($row['allowances'] == '') { ?>
            <div class="alert alert-info"><?php echo get_phrase('no_allowances');?></div>
        <?php } else { ?>
    <table class="empDetail1">
      <tr class="myBackground">
      <th colspan="2">
    EARNINGS
      </th>
      <th></th>
      <th class="table-border-right">Amount (Rs.)</th>
      <!-- <th colspan="2">Deductions</th>
      <th></th>
      <th>Amount (Rs.)</th> -->
      </tr>
       
        <th colspan="2">Basic Salary</th>              
      
      
      <td></td>
      <td class="myAlign"><?php echo $user->joining_salary; ?></td>
      <!-- <th colspan="2" >TDS</th> -->
      <!-- <td></td> -->
       <!-- <td class="myAlign">00.00</td> -->
      </tr>
       <?php
          $total_allowance    = 0;
          $allowances         = json_decode($row['allowances']);
          $i = 1;
          foreach ($allowances as $allowance)
          {
              $total_allowance += $allowance->amount; ?>
      <tr>
      <th colspan="2"> <?php echo $allowance->type; ?></th>
      <td></td>
      <td class="myAlign">  <?php echo $allowance->amount; ?></td>
      <!-- <th colspan="2">PT</th> -->
      <!-- <td></td> -->
      <!-- <td class="myAlign">00.00</td> -->
      </tr>
        <?php }  ?>
    <!-- <th colspan="2">PHONE ALLOWANCE</th> -->
     <!-- <td></td> -->
      <!-- <td class="myAlign">00.00 -->
      </td>
              </tr >
              <tr>
              </tr >
              <tr>
              </tr >
            </table>
             <?php } ?>

              <?php if ($row['deductions'] == '') { ?>
            <div class="alert alert-info"><?php echo get_phrase('no_deductions'); ?></div>
        <?php } else { ?>

             <table class="empDetail1">
      <tr class="myBackground">
      <th colspan="2">
    <?php echo 'DEDUCTIONS'; ?>
      </th>
      <th></th>
      <th class="table-border-right"><?php echo get_phrase('amount'); ?>(Rs.)</th>
      <!-- <th colspan="2">Deductions</th>
      <th></th>
      <th>Amount (Rs.)</th> -->
      </tr>
      <?php
        $total_deduction = 0;
        $deductions = json_decode($row['deductions']);
        $i = 1;
        foreach ($deductions as $deduction) {
            $total_deduction += $deduction->amount;
            ?>
      <tr>
      <th colspan="2"> <?php echo $deduction->type; ?></th>
      <td></td>
      <td class="myAlign"> <?php echo $deduction->amount; ?></td>
      <!-- <th colspan="2" >TDS</th> -->
      <!-- <td></td> -->
       <!-- <td class="myAlign">00.00</td> -->
      </tr>
      <?php } ?>
    <!-- <th colspan="2">TOTAL</th> 
    <td></td>
    <td class="myAlign">00.00
    </td> -->
    <!-- <th colspan="2" >TOTAL</th> -->
      <!-- <td></td> -->
    <!-- <td class="myAlign">00.00 -->
    </td>
    </tr >
   <tr>
  <!--  <th colspan="2">SITE ALLOWANCE</th>
    <td></td>
     <td class="myAlign">00.00</td> -->
    </tr>
    <tr>
    <!-- <th colspan="2">PHONE ALLOWANCE</th> -->
     <!-- <td></td> -->
      <!-- <td class="myAlign">00.00 -->
      </td>
              </tr >
               <tr>
    <!-- <th colspan="2">PHONE ALLOWANCE</th> -->
     <!-- <td></td> -->
      <!-- <td class="myAlign">00.00 -->
      </td>
    </tr>
      <tr>
      <th colspan="2">Total Deductions</th>
      <td></td>
      <td class="myAlign"><?php echo $total_deduction; ?> </td>
      <!-- <th colspan="2" >ESI</th > -->
      <!-- <td></td> -->
      <!-- <td class="myAlign">00.00</td> -->
      </tr>
            </table>
              <?php } ?>
            <table class="empDetail">
               <tr class="myBackground">
                <th colspan="3">
                  TOTAL AMOUNT
               </th>
                <td class="myAlign">
                  <?php echo $user->joining_salary + $total_allowance; ?>
              </td>
              <th colspan="3">
                  AMOUNT PAYBLE
               </th>
                <td class="myAlign">
                  <?php
                    $net_salary = $user->joining_salary + $total_allowance - $total_deduction;
                    echo $net_salary; ?>
              </td>
              </tr >
              <!-- <tr>
                <th colspan="7">
                  <td class="text-left">
                    <button onclick="window.print()">Print this page</button>
                  </td>
                </th>
              </tr> -->
              </tbody>
            </table >
           <p><b> Note:</b> This is an electronic generated copy and does not require any Signature or any company seal.
If you are not the intended recipient you are not authorized to use or disclose it in any form.
If you received this in error please destroy it along with any copies and notify the sender immediately</p>
<div style="text-align: center;"> <button onclick="window.print()">Print</button></div>
          </div >


<script type="text/javascript">

      jQuery(document).ready(function ($)
    {
        var elem = $('#payroll_print');
        PrintElem(elem);
        Popup(data);

    });

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data)
    {
        var mywindow = window.open('', 'payroll', 'height=400,width=600');
        mywindow.document.write('<html><head><title>Payroll Details</title>');
        mywindow.document.write('<link rel="stylesheet" href="assets/css/neon-theme.css" type="text/css" />');
        mywindow.document.write('<link rel="stylesheet" href="assets/js/datatables/responsive/css/datatables.responsive.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.print();
        mywindow.close();

        return true;
    }

</script>