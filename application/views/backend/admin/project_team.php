<?php
$edit_data = $this->db->get_where('project_team', array('project_code' => $param2))->result_array();
?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <i class=""></i>
                        <?php echo get_phrase('team'); ?>
                    </div>
                </div>

                <div class="panel-body">
                    <?php
                    foreach($edit_data as $row) { 
                        //echo $row['team'];
                        $teamName = $this->db->get_where('user',array('user_code'=>$row['team']))->result_array();
                        foreach($teamName as $team)
                        {
                        ?>
                        <img src="<?php echo $this->crud_model->get_image_url('user', $team['user_code']); ?>" class="img-circle" width="50" />
                        <?php
                            echo $team['name'] . "<br/>";
                        }
                    } ?>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">

    $( document ).ready(function() {

        // SelectBoxIt Dropdown replacement
        if($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function(i, el)
            {
                var $this = $(el),
                    opts = {
                        showFirstOption: attrDefault($this, 'first-option', true),
                        'native': attrDefault($this, 'native', false),
                        defaultText: attrDefault($this, 'text', ''),
                    };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>
