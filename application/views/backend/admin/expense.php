<?php
     ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); 
?>
<style>
@media print {
  * {
    display: none;
  }
  #printableTable {
    display: block;
  }
}
</style>
<a href="javascript:;"  style = "margin-left: 10px;" onclick="showAjaxModal('<?php echo site_url('modal/popup/expense_add'); ?>');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_credit'); ?>
</a> 
 <a href="#" class="btn btn-primary pull-right" onclick="showAjaxModal('<?php echo site_url('modal/popup/expense_use/'); ?>');">
    <i class="entypo-pencil"></i>
<?php echo get_phrase('add_debit'); ?>
</a>
<br><br><br>
<input type="button" onclick="printDiv('printableArea')" value="print" />

<div id="printableArea" class = "table-responsive">
<table  class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div>Ref No.</div></th>
            <!--<th><div><?php echo get_phrase('received_date'); ?></div></th>-->
            <th><div><?php echo get_phrase('TXN_date'); ?></div></th>
            <th><div><?php echo get_phrase('receive_from'); ?></div></th>
            <!--<th><div><?php //echo get_phrase('expense_title'); ?></div></th>-->
            <th><div><?php echo get_phrase('particulars'); ?></div></th>
            <th><div><?php echo get_phrase('CREDIT'); ?></div></th>
            <th><div><?php echo get_phrase('DEBIT'); ?></div></th>
            <!--<th><div><?php //echo get_phrase('used_date'); ?></div></th>-->
            <th><div><?php echo get_phrase('mode_of_amount'); ?></div></th>
            <th><div><?php echo get_phrase('balance'); ?></div></th>
            
            <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        $this->db->order_by('expense_id', 'asc');
        $expense = $this->db->get('expense')->result_array();
        foreach ($expense as $row):
            if($row['used_date'])
            {
                $udate = date('d/m/Y', $row['used_date']);
            }
            else
            {
                $udate ="";
            }
            ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $row['expense_code']; ?></td>
                <!--<td><?php //echo date('d/m/Y', $row['date']); ?></td>-->
                <td><?php echo date('d/m/Y', $row['used_date']); ?></td>
                
                <td><?php echo $row['receive_from']; ?></td>
                <!--<td><?php //echo $row['title']; ?></td>-->
                <td><?php echo substr($row['description'], 0, 50) . '...'; ?></td>
                <td><?php echo $row['amount']; ?></td>
                <td><?php echo $row['amount_used']; ?></td>
                
                <!--<td><?php //echo $udate; ?></td>-->
                 <td><?php echo $row['mode_of_amount']; ?></td>
                <td colspan = ""  style="color: #019a53;font-family: cursive;font-size: 15px;"> <?php 

                        echo $row['balance_amount'];
                
                ?></td>
               
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/expense_edit/'.$row['expense_id']); ?>');">
                                    <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <li>
                                <a href="#" onclick="confirm_modal_hard_reload('<?php echo site_url('admin/expense/delete/'.$row['expense_id']); ?>');">
                                    <i class="entypo-trash"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>
            
    <?php endforeach; ?>
    </table>
    <table  class="table table-bordered datatable" id="table_export">
        <tbody>
            <tr>
                <td colspan = "5" style = "text-align:center;color: #0a4aff;font-family: cursive;font-size: 15px;"><b> <?php echo "<b>".get_phrase('Total')."</b>"; ?></b></td>
                <td colspan = "" style="color: #0a4aff;font-family: cursive;font-size: 15px;"> <?php 

                        $this->db->select('SUM(amount) AS AMOUNT', FALSE);
                        $query = $this->db->get('expense');
                         $result = $query->result();
                         //print_r($result);
                         
                         foreach($result as $amountArray)
                         {
                            echo "Total Credit -<br/> Rs. <b>".$amountArray->AMOUNT."</b>/-";
                         }
                
                ?></td>
                <td colspan = ""  style="color: #019a53;font-family: cursive;font-size: 15px;"> <?php 

                        $this->db->select('SUM(amount_used) AS AMOUNT', FALSE);
                        $query1 = $this->db->get('expense');
                         $result1 = $query1->result();
                         //print_r($result);
                         
                         foreach($result1 as $amountArray1)
                         {
                             $finalAmount =  $amountArray1->AMOUNT;
                            echo "Total Debit -<br/> Rs. <b>".$finalAmount."</b>/-";
                         }
                
                ?></td>
                
                 <td colspan = "2"></td>
                 <td colspan = ""  style="color: #019a53;font-family: cursive;font-size: 15px;"> <?php 

                        $this->db->select('SUM(amount_used) AS AMOUNT', FALSE);
                        $query2 = $this->db->get('expense');
                         $result2 = $query2->result();
                         //print_r($result);
                         
                         foreach($result2 as $amountArray2)
                         {
                             $finalAmount = $amountArray->AMOUNT - $amountArray2->AMOUNT;
                            echo "closing bal - <br/>Rs. <b>".$finalAmount."</b>/-";
                         }
                
                ?></td>
                 <td colspan= "2">
                     <a href="#" class = "btn btn-default" onclick="showAjaxModal('<?php echo site_url('modal/popup/carry_forward/'.$row['expense_id']); ?>');">
                                    <i class=""></i>
                                <?php echo get_phrase('carry_forward'); ?>
                                </a>
                     
                 </td>
            </tr>
    </tbody>
</table>

</div>

<script>
    function printDiv(divName) {
         var printContents = document.getElementById(divName).innerHTML;
         var originalContents = document.body.innerHTML;
    
         document.body.innerHTML = printContents;
    
         window.print();
    
         document.body.innerHTML = originalContents;
    }
</script>
<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(7, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>
