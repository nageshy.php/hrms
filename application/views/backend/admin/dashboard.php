<?php

// ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); 
$logged_in_user = $this->db->get_where('user', array('user_id' => $this->session->userdata('login_user_id')))->row(); 


 //$conn = @mysqli_connect('localhost','svsktbgs_demohrm','demohrms','svsktbgs_demohrms');
 $conn = $this->db->conn_id;
//  if($conn)
//  {
//      echo "connected";
//  }
// if($conn)
// {
//     //date_default_timezone_set("Asia/Kolkata"); 
//   //echo "success"; 
//   $ip = getenv('REMOTE_ADDR');
//     $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
//     ;
    
//     $city = $geo["geoplugin_city"];
//     $latitude=$geo["geoplugin_latitude"];
//     $longitude=$geo["geoplugin_longitude"];
//     $region = $geo["geoplugin_regionName"];
//     $date = date('Y-m-d H:i:s',time());
//     $onlydate = date('Y-m-d',time());
//      $sql = "insert into user_log(user_id,ip,city,region,lattitude,longitude,created_at,date,status)values($userId,'".$ip."','".$city."','".$region."','".$latitude."','".$longitude."','".$date."','".$onlydate."',1)";
//     $result = @mysqli_query($conn,$sql);
//     if($result)
//     {
//         echo "Login IP : ". $ip;
        
//     }
//     else
//     {
//         echo "IP failed";
//     }
 
// }
// else
// {
//     //echo "failed";
// }



?>


<!-- Chart code -->
<script>
am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create("chartdiv1", am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = [
  {
    country: "JAN",
    visits: 23725
  },
  {
    country: "FEB",
    visits: 1882
  },
  {
    country: "MARCH",
    visits: 1809
  },
  {
    country: "APR",
    visits: 1322
  },
  {
    country: "MAY",
    visits: 1122
  },
  {
    country: "JUNE",
    visits: 1114
  },
  {
    country: "JULY",
    visits: 984
  },
  {
    country: "AUG",
    visits: 711
  },
  {
    country: "SEPT",
    visits: 665
  },
  {
    country: "OCT",
    visits: 580
  },
  {
    country: "NOV",
    visits: 443
  },
  {
    country: "DEC",
    visits: 441
  }
];

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.renderer.grid.template.location = 0;
categoryAxis.dataFields.category = "country";
categoryAxis.renderer.minGridDistance = 40;
categoryAxis.fontSize = 11;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 24000;
valueAxis.strictMinMax = true;
valueAxis.renderer.minGridDistance = 30;
// axis break
var axisBreak = valueAxis.axisBreaks.create();
axisBreak.startValue = 2100;
axisBreak.endValue = 22900;
//axisBreak.breakSize = 0.005;

// fixed axis break
var d = (axisBreak.endValue - axisBreak.startValue) / (valueAxis.max - valueAxis.min);
axisBreak.breakSize = 0.05 * (1 - d) / d; // 0.05 means that the break will take 5% of the total value axis height

// make break expand on hover
var hoverState = axisBreak.states.create("hover");
hoverState.properties.breakSize = 1;
hoverState.properties.opacity = 0.1;
hoverState.transitionDuration = 1500;

axisBreak.defaultState.transitionDuration = 1000;
/*
// this is exactly the same, but with events
axisBreak.events.on("over", function() {
  axisBreak.animate(
    [{ property: "breakSize", to: 1 }, { property: "opacity", to: 0.1 }],
    1500,
    am4core.ease.sinOut
  );
});
axisBreak.events.on("out", function() {
  axisBreak.animate(
    [{ property: "breakSize", to: 0.005 }, { property: "opacity", to: 1 }],
    1000,
    am4core.ease.quadOut
  );
});*/

var series = chart.series.push(new am4charts.ColumnSeries());
series.dataFields.categoryX = "country";
series.dataFields.valueY = "visits";
series.columns.template.tooltipText = "{valueY.value}";
series.columns.template.tooltipY = 0;
series.columns.template.strokeOpacity = 0;

// as by default columns of the same series are of the same color, we add adapter which takes colors from chart.colors color set
series.columns.template.adapter.add("fill", function(fill, target) {
  return chart.colors.getIndex(target.dataItem.index);
});

}); // end am4core.ready()
</script>
<style>
@media print {
  * {
    display: none;
  }
  #printableTable {
    display: block;
  }
}

  .bdg { 
        position: relative; 
        top: -20px; 
        left: -25px; 
        border: 1px solid black; 
        border-radius: 50%; 
       } 

        .notification {
                    position: absolute !important;
                    background-color: #555;
                    color: white;
                    text-decoration: none;
                    padding: 8px 19px;
                    position: relative;
                    display: inline-block;
                    border-radius: 4px;
                    margin-left: -31px;
                    top: -50px;
                    left: 262px;
        }

        .notification:hover {
                background: #00c0ef;
                color: #fff;
            }

        .notification .bdg-leave {
          position: absolute;
          top: -10px;
          right: -10px;
          padding: 5px 10px;
          border-radius: 50%;
          background-color: red;
          color: white;
        }
        input.btn.aqua-gradient.btn-rounded.btn-sm.my-0 {
    font-family: inherit;
    background: linear-gradient(40deg,#ff6ec4,#7873f5) !important;
    color: #fff;
    text-transform: uppercase;
    position: relative;
    overflow: hidden;
    box-shadow: 0 2px 5px 0 rgba(0,0,0,0.16), 0 2px 10px 0 rgba(0,0,0,0.12);
    -webkit-transition: color .15s;
}
  .text-center {
    text-align: center;
    color: #fff;
    font-size: 20px;
    font-family: cursive;
}  
ul.leave-li li {
    list-style: none;
    margin-right: 35px;
       color: #ffffff;
    float: left;
   
}
ul.leave-li1 li {
    list-style: none;
    margin-right: 35px;
       color: #ffffff;
    float: left;
   
}
.leave-li1
{
/*background-image: linear-gradient(90deg, #705b8e, #715125,#4f619a); */
    background-color: #3a434e !important;
    position: absolute;
    z-index: 999;
    height: 35px;
    padding-top: 9px;
}
.leave-bg
{
    background-image: linear-gradient(90deg, #a571ec, #bb9564,#1d6084);
    /*position: absolute;*/
    /*bottom: 6px;*/
    margin-top: -15px;
    width: 97%;
}
</style>
<script>
// $(document).ready(function(){
//     get_location();
// });

// function get_location() {
//     if (Modernizr.geolocation) {
//         navigator.geolocation.getCurrentPosition(register_coords);
//     } else {
//         // no native support; maybe try Gears?
//     }
// }

// function register_coords(position) {
//     var latitude = position.coords.latitude;
//     var longitude = position.coords.longitude;

//     // use jquery or your own preference to send the values to CI
//     $.post('".base_url()."/login/validate_login', { latitude:latitude, longitude:longitude }, function(){
//         // some optional callback
//     });
// }
</script>
<div class="row background-half">

<!-- Modal -->
<!-- <div id="myModal" class="modal fade in" style="top: 20%; display: block;">-->
<!--<div class="modal-dialog">-->
<!--<div class="modal-content" style="background-color:#fff0!important">-->
<!--<div class="modal-header" style="padding:0!important;margin-bottom:-41px">-->
<!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="z-index:999;margin-top:4px;position:relative;left:0px;top:-13px">×</button>-->
<!--</div>-->
<!--<div class="modal-body" style = "background-color: #3d5a9a;">-->
<!--<div class="text-center">-->
<!--  We are  Upgraded!!! (Version No.2)<br/>-->
<!--  ------------------------ <br/>-->
<!--  Please Check the Updates <br/>-->
<!--  1.Projects <br/>-->
<!--  2.Work Report <br/>-->
<!--  3.Support <br/>-->
<!--  4.Dashboard <br/>-->
<!--  </p>-->
<!--</div>-->
<!--<form>-->
<!--</form>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->


	<div class="col-md-12" style="text-align:center;">
        <?php 
        
            $roles = $this->db->get_where('user',array('user_id'=>$this->session->userdata('login_user_id')))->row();
            if($roles->role == 1)
            {
                $leaveNotify = $this->db->get_where('leave', array('status' => null))->num_rows(); 
                if($leaveNotify > 0)
                {
                    echo "<a href = '".base_url()."admin/leave' class='notification'>".'<span class="bdg-leave">'.$leaveNotify."</span>Leaves</a>";
                }
                else
                {
                    
                }
            }
            
            ?>
    	<!--<img src="<?php echo $this->crud_model->get_image_url('admin', $this->session->userdata('login_user_id')); ?>" alt="" class="img-circle" style="height:60px;">-->

    	<!--<br>-->

    	<!--<span style="font-size: 20px; font-weight: 100;">-->

    	<!--	<?php echo get_phrase('welcome'); ?>,-->

	    <!--	<span style="font-size: 30px; font-weight: 300;">-->

	    <!--		<?php echo $logged_in_user->name; ?>-->

	    <!--	</span>-->

     <!--       <br>-->

     <!--       <?php echo date ('d/m/Y'); ?>-->

    	<!--</span>-->
              <div class="welcome-box"><div class="welcome-img">
	        <img alt="" src="<?php echo $this->crud_model->get_image_url('admin', $this->session->userdata('login_user_id')); ?>" alt="<?php echo $logged_in_user->name; ?>" class="img-circle1" /">
	        </div><div class="welcome-det"><h3>Welcome, <?php echo $logged_in_user->name; ?></h3><p><?php echo $logged_in_user->user_code; ?></p>
	        <p> <?php echo date ('d/m/Y'); ?></p></div></div>
	</div>

    

	<div class="col-md-12" style="margin-top:10px;">



		<div class="row" style = "    margin-bottom: 20px;">



			<div class="col-md-4">

            

                <div class="tile-stats tile-red">

                    <div class="icon"><i class="entypo-users"></i></div>

                    <div class="num" data-start="0" data-end="<?php echo $this->db->get_where('user', array('type' => 2))->num_rows(); ?>" 

                    		data-postfix="" data-duration="1500" data-delay="0">0</div>

                    

                    <a href = "<?php echo base_url() . 'admin/employee/list';?>"><h3><?php echo get_phrase('total_employee');?></h3></a>

                </div>



            </div>



            <div class="col-md-4">



                <div class="tile-stats tile-green">

                    <div class="icon"><i class="entypo-chart-area"></i></div>

                    <div class="num" data-start="0" data-end="<?php echo $this->db->get_where('attendance', array('date' => strtotime(date('m/d/Y')), 'status' => 1))->num_rows(); ?>" 

                    		data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/attendance_popup_view/'); ?>');">

                    <h3><?php echo get_phrase('present_today');?></h3></a>

                </div>

                

            </div>



            <?php

            $on_leave           = 0;

            $current_date       = strtotime(date('m/d/Y'));

            $approved_leaves    = $this->db->get_where('leave', array('status' => 1))->result_array();
           
            
            foreach($approved_leaves as $row)
            
                if($current_date >= $row['start_date'] && $current_date <= $row['end_date'])

                    $on_leave++;
                   

            ?>



            <div class="col-md-4">

            

                <div class="tile-stats tile-aqua">

                    <div class="icon"><i class="entypo-flight"></i></div>

                    <div class="num" data-start="0" data-end="<?php echo $on_leave; ?>" 

                    		data-postfix="" data-duration="1500" data-delay="0">0</div>

                    

                    <h3><?php echo get_phrase('on_leave');?></h3>

                </div>



            </div>



		</div>
	
        <div class = "col-md-12">
        	<?php 
		  //$countL =  $approved_leaves->num_rows();
		  //echo $on_leave;
		  if($on_leave != 0)
		  {
		?>
            <div class = "leave-bg">
            <ul class = "leave-li1">
                <li>On Leave Employees : </li>
                </ul>
                <ul class = "leave-li">
                <marquee>
            <?php
            foreach($approved_leaves as $leaveNameArray)
            {
                if($current_date >= $leaveNameArray['start_date'] && $current_date <= $leaveNameArray['end_date'])
                {
                    $lId = $leaveNameArray['user_id'];
                    $userCode = $leaveNameArray['user_code'];
                    
                    $usersql = $this->db->get_where('user',array('user_id'=>$lId))->row();
                    $lname = $usersql->name;
                    
                    echo "<li>";?>
                    <img alt="" src="<?php echo  $this->crud_model->get_image_url('user', $userCode); ?>" alt="<?php echo $logged_in_user->name; ?>" class="img-circle" /" width = '30px'>
                  <?php echo $lname."</li>";
                    
                }
                
            }
            ?>
            </marquee>
            </ul>
            <?php
		         }
            ?>
            </div>
            
        </div>
		<div class="row">
		<div class="col-md-6 col-xs-6" >
        <div id="chartdiv"></div>
    </div>
    <div class="col-md-6 col-xs-6" >
        <div id="chartdiv1"></div>
    </div>
        <?php
             if($roles->role == 1)
            {
        ?>


            <div class="col-md-12 col-xs-12" style=" border: 1px solid;box-shadow: 1px 1px 14px 5px #ddd; padding: 8px;"> 
             <div class= "col-md-6 col-xs-6">
                <form action = '' method = 'post'>
                    <div class="col-md-12" style = "text-align:center;margin-bottom:10px;">
                       <span style="font-weight: 900;font-size: 20px;color: #5e85bb;">Search By Date</span>
                    </div>
                     <div class="col-md-6">
                        <input type = 'date' class="form-control" name = 'selectdate'>
                    </div>
                     <div class="col-md-6">
                        <input type = 'submit' value = 'search' class="form-control btn aqua-gradient btn-rounded btn-sm my-0" name = 'submit'/>
                    </div>
                </form>
                 <button class="Button Button--outline" onclick="printDiv()"  style='width: 104px;padding: 5px;position: absolute;left: 567px;float: right;top: 39px;background: linear-gradient(40deg,#170073,#b2b0ef) !important;/* background-color: #f56954; */color: #fff;border: 1px solid #281380;box-shadow: 1px 1px 16px 7px #ddd;'>
                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Print Report</button>
                </div>
                <?php
                    $sub = $this->input->post('submit');
                    // $empdata = $this->db->get_where('user_log', array('status' => 1))->result_array();
                    if($sub)
                    {
                        $curTime = $this->input->post('selectdate');
                        //$curTime = '2020-09-24';
                        //$date = date('Y-m-d H:i:s',time());
                        if($curTime == '')
                        {
                            $curTime = date('Y-m-d',time());
                        }
                    }
                    else
                    {
                        $curTime = date('Y-m-d',time());
                        //$date = date('Y-m-d H:i:s',time());
                    }
                    
                   $empdata = "select * from user_log where status = 1 and date = '$curTime' group by user_id";
                    $empres = @mysqli_query($conn,$empdata);
                   
                     
                    $totalAtt = @mysqli_num_rows($empres);
                      
                   
                ?>
              
               <div class = "col-md-12">
                 <div id="printableTable">
                <h2>Emplyees Login Status <?php echo " ~ <b>".date('d-m-yy',strtotime($curTime)) . "</b><b><small  style='float: right;color: #00acd6;'> (Total Logins - ". $totalAtt . ") </small></b>"; ?></h2>
                <div id="table_export_wrapper" class="table-responsive dataTables_wrapper form-inline no-footer">
                <table class="table table-bordered datatable " id="table_export">
                  <thead>
                    <tr>
                      <th scope="col">Name </th>
                      <th scope="col">Login City</th>
                      <th scope="col">Last Login Time</th>
                      <th scope="col">Last Logout Time</th>
                      <th scope="col">Working Hours</th>
                      <th scope="col">Break Time</th>
                      <th scope="col">Status</th>
                      <th scope="col">Login Type</th>
                    </tr>
                  </thead>
                  <tbody>
                      <? 
                      $i=1;
                      foreach ($empres as $empval) {
                          $usrid = $empval['user_id'];
                          $empS = "select * from user where user_id = $usrid ";
                      $empr = @mysqli_query($conn,$empS);
                      foreach($empr as $emprs)
                      {
                          $cdate = strtotime ($empval['created_at']);
                           $loginTime = date('h:i:s',$cdate);
                           $cdate = date('yy-m-d',$cdate);
                           $ldate = date('yy-m-d',time());
                            $userCode = $emprs['user_code'];
                         
                        //  if($cdate != $ldate)
                        //  {
                             
                      ?>  
                    <tr>
                        <th scope="row"  style=" font-size: 11px;text-transform: lowercase;">
                      
                 <img alt="" src="<?php echo  $this->crud_model->get_image_url('user', $userCode); ?>" alt="<?php echo $logged_in_user->name; ?>" class="img-circle" /" width = '30px'>
                          <?php
                             echo  " " . $emprs['name'];
                            //echo $this->session->userdata('token');
                             //echo substr($emprs['name'], 0, 15);
                            
                          ?>
                      </th>
                      <td><?php echo  $empval['city']; ?> </td>
                      <td><?php echo  $loginTime; ?> </td>
                      <td><?php 
                      
                         $sqlLogout = "select * from user_log where status = 1 and date = '$curTime' and user_id = $usrid order by logout_time desc limit 1";
                        $resL = @mysqli_query($conn,$sqlLogout);
                        
                        foreach($resL as $rowArray)
                        {
                            $logoutTime = $rowArray['logout_time'];
                            
                              $logoutTime1 = $rowArray['logout_date_time'];
                             $logoutc = strtotime ($logoutTime1);
                              $logoutfTime = date('yy-m-d h:i:s',$logoutc);
                             
                               $totTime =  $cdate-$logoutc;
                               $loginStatus = $rowArray['login_status'];
                            
                            //echo round($totTime/60)." minutes ".($totTime%60)." seconds";
                            
                            $logout = strtotime($rowArray['logout_date_time']);
                            $login = strtotime($empval['created_at']);
                            $diff=$logout-$login;
                            $hours = floor($diff / 60 / 60);
                            $minutes    = round(($diff - ($hours * 60 * 60)) / 60);
                            $totalWtime = round($diff/60)." minutes ".($diff%60)." seconds";
                            
                        $i++;
                        //echo  $empval['logout_time']; 
                        
                        if($logoutTime1 == '')
                        {
                            
                        }
                        else
                        {
                            echo $logoutTime;
                        }
                        
                        ?>
                      
                      </td>
                      <td>
                          <?php 
                          if($logoutTime1)
                          {
                                //$this->db->order_by('breakin', 'desc');
                                 //$this->db->limit(1);
                                 $breaktime = $this->db->get_where('user_log', array('user_id' => $usrid,'date'=>$curTime))->result_array();
                                 foreach($breaktime as $brin)
                                 {
                                     $brinv = $brin['breakin'];
                                     
                                 }
                                 
                                 $this->db->order_by('breakout', 'desc');
                                 //$this->db->limit(1);
                                 $breakouttime = $this->db->get_where('user_log', array('user_id' => $usrid,'date'=>$curTime))->result_array();
                                 foreach($breakouttime as $brout)
                                 {
                                     $broutv = $brout['breakout'];
                                     
                                 }
                        
                            //echo $brinv."<br/>".$broutv."";
                             echo $hours.' hours.'.$minutes." minutes";// echo $minutes;
                          }
                          } ?>
                          
                      </td>
                       <td>
                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/break_time/'.$usrid); ?>');">
                            <i class="entypo-eye"></i>
                            <?php echo get_phrase('view'); ?>
                        </a>
                          
                      </td>
                      <td>
                          <?php
                            $loginS = $this->db->get_where('user',array('user_id'=>$usrid))->result_array();
                            foreach($loginS as $loginstatus)
                            {
                                //echo $loginstatus['login_status'];
                                if($loginstatus['login_status'] == 1)
                                {
                                    echo "<b style = 'height: 15px;width: 15px;background-color: #12af42;border-radius: 50%;display: inline-block;'></b> Online";
                                }
                                else
                                {
                                     echo "<b style = 'height: 15px;width: 15px;background-color: #fd2808;border-radius: 50%;display: inline-block;'></b> Ofline";
                                }
                                
                            }
                          ?>
                      </td>
                      <td>
                      <?php if($loginStatus == '1')
                      {
                      ?>
                      <!--<td><b  style="border: 1px solid #12af42;background-color: #12af42;padding: 5px;color: #fff;box-shadow: -1px -1px 11px 3px #ddd;border-radius: 10px;">Logged In</b>-->
                      <?php //echo " " . $empval['ip']; 
                        if($empval['ip'] == "103.211.39.162")
                        {
                            echo " - <b> Office Login</b> ";
                        }
                        else
                        {
                            $lat = $empval['lattitude'];
                            $long = $empval['longitude'];
                            echo "- <b style = 'color:red'> Out Side Login - </b>". $empval['ip'];
                            echo "<a href = 'https://maps.google.com/?q=$lat,$long' style = 'color:blue' target='_blank'> Track Location</a>";
                        }
                      ?>
                      </td>
                      <?php }else if($loginStatus == '2')
                        {
                      ?>
                       <!--<td><b  style="border: 1px solid orange;background-color: orange;padding: 5px;color: #fff;box-shadow: -1px -1px 11px 3px #ddd;border-radius: 10px;">Logged Out</b>-->
                      <?php //echo " " . $empval['ip']; 
                        if($empval['ip'] == "103.211.39.162")
                        {
                            //echo " - <b> Office Login</b> ";
                        }
                        else
                        {
                            $lat = $empval['lattitude'];
                            $long = $empval['longitude'];
                            //echo "- <b style = 'color:red'> Out Side Login - </b>". $empval['ip'];
                            //echo "<a href = 'https://maps.google.com/?q=$lat,$long' style = 'color:blue' target='_blank'> Track Location</a>";
                        }
                      ?>
                      </td>
                    </tr>
                   <?php  } //}
                   else
                   {
                       echo "No Results Found ";
                   }
                   
                   }  } ?>
                  </tbody>
                </table>
                
                </div>
                </div>
                <?php } ?>
                
                <!--TL Options-->
                
                
                
                 <?php
             if($roles->role == 5)
            {
        ?>


            <div class="col-md-12 col-xs-12" style=" border: 1px solid;box-shadow: 1px 1px 14px 5px #ddd; padding: 8px;">    
                <form action = '' method = 'post'>
                    <div class="col-md-12" style = "text-align:center;margin-bottom:10px;">
                       <span style="font-style: italic;font-weight: 900;font-size: 20px;color: #5e85bb;">Search By Date</span>
                    </div>
                     <div class="col-md-6">
                        <input type = 'date' class="form-control" name = 'selectdate'>
                    </div>
                     <div class="col-md-6">
                        <input type = 'submit' value = 'search' class="form-control btn aqua-gradient btn-rounded btn-sm my-0" name = 'submit'/>
                    </div>
                </form>
                <?php
                    $sub = $this->input->post('submit');
                    // $empdata = $this->db->get_where('user_log', array('status' => 1))->result_array();
                    if($sub)
                    {
                        $curTime = $this->input->post('selectdate');
                        //$curTime = '2020-09-24';
                        //$date = date('Y-m-d H:i:s',time());
                        if($curTime == '')
                        {
                            $curTime = date('Y-m-d',time());
                        }
                    }
                    else
                    {
                        $curTime = date('Y-m-d',time());
                        //$date = date('Y-m-d H:i:s',time());
                    }
                    
                   $empdata = "select * from user_log where status = 1 and date = '$curTime' group by user_id";
                    $empres = @mysqli_query($conn,$empdata);
                   
                     
                    $totalAtt = @mysqli_num_rows($empres);
                      
                   
                ?>
                <button class="Button Button--outline" onclick="printDiv()"  style="position: relative;left: 454px;top: 39px;background: linear-gradient(40deg,#170073,#b2b0ef) !important;/* background-color: #f56954; */color: #fff;border: 1px solid #281380;box-shadow: 1px 1px 16px 7px #ddd;">
                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Print Report</button>
                 <div id="printableTable">
                <h2>Employees Login Status <?php echo " ~ <b>".date('d-m-yy',strtotime($curTime)) . "</b><b><small  style='float: right;color: #00acd6;'> (Total Logins - ". $totalAtt . ") </small></b>"; ?></h2>
                <div id="table_export_wrapper" class="dataTables_wrapper form-inline no-footer">
                <table class="table table-bordered datatable table-responsive" id="table_export">
                  <thead>
                    <tr>
                        
                      <th scope="col">Name </th>
                      <th scope="col">Login City</th>
                      <th scope="col">Last Login Time</th>
                      <th scope="col">Last Logout Time</th>
                      <th scope="col">Working Hours</th>
                      <th scope="col">Break Time</th>
                      <th scope="col">Status</th>
                      <th scope="col">Login Type</th>
                    </tr>
                  </thead>
                  <tbody>
                      <? 
                      $i=1;
                      foreach ($empres as $empval) {
                          $usrid = $empval['user_id'];
                         
                          $empS = "select * from user where user_id = $usrid";
                      $empr = @mysqli_query($conn,$empS);
                      foreach($empr as $emprs)
                      {
                          $cdate = strtotime ($empval['created_at']);
                           $loginTime = date('h:i:s',$cdate);
                           $cdate = date('yy-m-d',$cdate);
                           $ldate = date('yy-m-d',time());
                           
                            $userCode = $emprs['user_code'];
                         
                        //  if($cdate != $ldate)
                        //  {
                             
                      ?>  
                    <tr>
                       
                      <th scope="row"  style=" font-size: 11px;text-transform: lowercase;">
                      
                 <img src="<?php echo  $this->crud_model->get_image_url('user', $userCode); ?>" alt="<?php echo $logged_in_user->name; ?>" class="img-circle" /" width = '30px'>
                          <?php
                             echo  " " . $emprs['name'];
                            //echo $this->session->userdata('token');
                             //echo substr($emprs['name'], 0, 15);
                            
                          ?>
                      </th>
                      <td><?php echo  $empval['city']; ?> </td>
                      <td><?php echo  $loginTime; ?> </td>
                      <td><?php 
                      
                         $sqlLogout = "select * from user_log where status = 1 and date = '$curTime' and user_id = $usrid order by logout_time desc limit 1";
                        $resL = @mysqli_query($conn,$sqlLogout);
                        
                        foreach($resL as $rowArray)
                        {
                            $logoutTime = $rowArray['logout_time'];
                            
                              $logoutTime1 = $rowArray['logout_date_time'];
                             $logoutc = strtotime ($logoutTime1);
                              $logoutfTime = date('yy-m-d h:i:s',$logoutc);
                             
                               $totTime =  $cdate-$logoutc;
                               $loginStatus = $rowArray['login_status'];
                            
                            //echo round($totTime/60)." minutes ".($totTime%60)." seconds";
                            
                            $logout = strtotime($rowArray['logout_date_time']);
                            $login = strtotime($empval['created_at']);
                            $diff=$logout-$login;
                            $hours = floor($diff / 60 / 60);
                            $minutes    = round(($diff - ($hours * 60 * 60)) / 60);
                            $totalWtime = round($diff/60)." minutes ".($diff%60)." seconds";
                            
                        $i++;
                        //echo  $empval['logout_time']; 
                        
                        if($logoutTime1 == '')
                        {
                            
                        }
                        else
                        {
                            echo $logoutTime;
                        }
                        
                        ?>
                      
                      </td>
                      <td>
                          <?php 
                          if($logoutTime1)
                          {
                                //$this->db->order_by('breakin', 'desc');
                                 //$this->db->limit(1);
                                 $breaktime = $this->db->get_where('user_log', array('user_id' => $usrid,'date'=>$curTime))->result_array();
                                 foreach($breaktime as $brin)
                                 {
                                     $brinv = $brin['breakin'];
                                     
                                 }
                                 
                                 $this->db->order_by('breakout', 'desc');
                                 //$this->db->limit(1);
                                 $breakouttime = $this->db->get_where('user_log', array('user_id' => $usrid,'date'=>$curTime))->result_array();
                                 foreach($breakouttime as $brout)
                                 {
                                     $broutv = $brout['breakout'];
                                     
                                 }
                        
                            echo $brinv."<br/>".$broutv."";
                             //echo $hours.' hours.'.$minutes." minutes";// echo $minutes;
                          }
                          } ?>
                          
                      </td>
                       <td>
                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/break_time/'.$usrid); ?>');">
                            <i class="entypo-eye"></i>
                            <?php echo get_phrase('view'); ?>
                        </a>
                          
                      </td>
                      <td>
                          <?php
                            $loginS = $this->db->get_where('user',array('user_id'=>$usrid))->result_array();
                            foreach($loginS as $loginstatus)
                            {
                                //echo $loginstatus['login_status'];
                                if($loginstatus['login_status'] == 1)
                                {
                                    echo "<b style = 'height: 15px;width: 15px;background-color: #12af42;border-radius: 50%;display: inline-block;'></b> Online";
                                }
                                else
                                {
                                     echo "<b style = 'height: 15px;width: 15px;background-color: #fd2808;border-radius: 50%;display: inline-block;'></b> Ofline";
                                }
                                
                            }
                          ?>
                      </td>
                      <td>
                      <?php if($loginStatus == '1')
                      {
                      ?>
                      <!--<td><b  style="border: 1px solid #12af42;background-color: #12af42;padding: 5px;color: #fff;box-shadow: -1px -1px 11px 3px #ddd;border-radius: 10px;">Logged In</b>-->
                      <?php //echo " " . $empval['ip']; 
                        if($empval['ip'] == "103.211.39.162")
                        {
                            echo " - <b> Office Login</b> ";
                        }
                        else
                        {
                            $lat = $empval['lattitude'];
                            $long = $empval['longitude'];
                            echo "- <b style = 'color:red'> Out Side Login - </b>". $empval['ip'];
                            echo "<a href = 'https://maps.google.com/?q=$lat,$long' style = 'color:blue' target='_blank'> Track Location</a>";
                        }
                      ?>
                      </td>
                      <?php }else if($loginStatus == '2')
                        {
                      ?>
                       <!--<td><b  style="border: 1px solid orange;background-color: orange;padding: 5px;color: #fff;box-shadow: -1px -1px 11px 3px #ddd;border-radius: 10px;">Logged Out</b>-->
                      <?php //echo " " . $empval['ip']; 
                        if($empval['ip'] == "103.211.39.162")
                        {
                            //echo " - <b> Office Login</b> ";
                        }
                        else
                        {
                            $lat = $empval['lattitude'];
                            $long = $empval['longitude'];
                            //echo "- <b style = 'color:red'> Out Side Login - </b>". $empval['ip'];
                            //echo "<a href = 'https://maps.google.com/?q=$lat,$long' style = 'color:blue' target='_blank'> Track Location</a>";
                        }
                      ?>
                      </td>
                    </tr>
                   <?php  } //}
                   else
                   {
                       echo "No Results Found ";
                   }
                   
                   }  } ?>
                  </tbody>
                </table>
                </div>
                </div>
                <?php } ?>
                
                
                <!--TL Option Ends Here-->
                
                
                
               
 <iframe name="print_frame" width="0" height="0" frameborder="0" src="about:blank"></iframe>
 <div class="col-md-6 col-xs-12" >    
                <div class="col-md-12" style="border:1px solid #ddd;border-radius:10px">                
                <h2>Timesheet</h2>
                    <h4 style="padding-bottom:20px">Date: <span><?php echo date ('d/m/Y'); ?></span></h4>
                <?php
                
                    // $empdata = $this->db->get_where('user_log', array('status' => 1))->result_array();
                    $curTime = date('Y-m-d',time());
                    $date = date('Y-m-d H:i:s',time());
                     $empdata = "select * from user_log where status = 1 and date = '$curTime' and user_id = $userId group by user_id ";
                    $empres = @mysqli_query($conn,$empdata);
                   
                     
                    $totalAtt = @mysqli_num_rows($empres);
                      
                   
                ?>
                <div class="punch-hours">
                    <? foreach ($empres as $empval) {
                          $usrid = $empval['user_id'];
                         $empS = "select * from user where user_id = $usrid ";
                      $empr = @mysqli_query($conn,$empS);
                      foreach($empr as $emprs)
                      {
                          $cdate = strtotime($empval['created_at']);
                           $ltime = date(' h:i:s a',$cdate);
                           $cdate = date('yy-m-d',$cdate);
                           $ldate = date('yy-m-d',time());
                           
                           $an = strtotime($empval['created_at']);
                           //$ltime = date('yy-m-d',strtotime ($empval['created_at']));
                         
                         if($cdate == $ldate)
                         {
                             
                      ?>  
                      <!--<h4><?php echo $ltime; ?></h4>-->
                    <h4><div class="digital-clock">00:00:00</div></h4>
                    
                    
                    
                     <?php }
                   else
                   {
                       //echo "No logins ";
                   }
                   
                   }} ?>
                </div>
                <div style="text-align:center;padding-top:15px;padding-bottom:10px">
                
                <!--<form action = ""method="post">-->
                <!--    <input id="punchinval" type="hidden" value = "<?php echo time();?>" name="punchin">-->
                <!--<input id="punchin" type="submit" class="btn btn-success btn-lg" value="Punch In" style="border-radius:50px;font-size: 20px;display:none">-->
                <!--</form>-->
                    <h3></h3>
                  
                   <a  href = "#" class="btn btn-success btn-lg" value="Punch Out" style="border-radius:50px;font-size: 20px;margin-bottom:10px">Punch In Time - <span><?php echo $ltime; ?></span></a>
                <a  href = "<?php echo site_url('login/logout'); ?>" class="btn btn-danger btn-lg" value="Punch Out" style="border-radius:50px;font-size: 20px;margin-bottom:10px">Punch Out</a>
                    
                
                </div>
                <div style="text-align:center;padding-top:10px;padding-bottom:20px;display:none;">
                
               <?php
                    $breaktimeToken = $this->session->userdata('breaktime_token');
                    $break = $this->db->get_where('user_log',array('date'=>$curTime,'breaktime_token'=>$breaktimeToken))->row();
                    $breakToken = $break->breaktime_status;
                 
                    if($breakToken == '0')
                    {
               ?>
                    
                    
                    <a href = "<?php echo site_url('employee/breaktime/add'); ?>" id = "break" class="btn btn-info btn-lg" style="border-radius:10px">Break In </a>
                    <?php }
                          else
                          {
                    ?>
                    <a href = "<?php echo site_url('employee/breaktime/update'); ?>" id = "break" class="btn btn-info btn-lg" style="border-radius:10px">Break Out</a>
                    <?php }?>
                    <input type="button" class="btn btn-info btn-lg" value="Overtime" style="border-radius:10px">
                </div>
                </div>
            </div>
            
            
               <div class="col-md-6 col-xs-12">
                 <div class="col-md-12" style="border:1px solid #ddd;border-radius:10px;text-align: center;">
                  <h2 style="padding-bottom:10px">Today Activity</h2>
                  </div>
                <div class="col-md-6" style="border:1px solid #ddd;border-radius:10px">
               
                <ul class="timeline">
                    <?php
                        $this->db->order_by('created_at', 'desc');
                        $this->db->limit(6);
                        $activity = $this->db->get_where('user_log', array('user_id' => $this->session->userdata('login_user_id'),'status' => 1,'date'=>$curTime))->result_array();
                        foreach($activity as $activitylog1)
                        {
                            
                    ?>
						<li>
							<p class="mb-0" style="margin:0px 0px 7px;color:black">Punch In at</p>
							<p class="res-activity-time">
								<i class="fa fa-clock-o"></i>
								<?php
								    $actlog = strtotime($activitylog1['created_at']);
                                    echo $logtime = date(' h:i:s a',$actlog); 
								?>
							</p>
						</li>
						<?php
                          }   
						?>
						</ul>
						</div>
						<div class="col-md-6" style="border:1px solid #ddd;border-radius:10px">
               
			<ul class="timeline">	
                    <?php
                        $this->db->order_by('logout_date_time', 'desc');
                        $this->db->limit(2);
                        $activity1 = $this->db->get_where('user_log', array('user_id' => $this->session->userdata('login_user_id'),'status' => 1,'date'=>$curTime))->result_array();
                        
                        foreach($activity1 as $activitylog)
                        {
                            if($activitylog['logout_date_time'] != '')
                            {
                    ?>
						<li>
							<p class="mb-0" style="margin:0px 0px 7px;color:black">Punch Out at</p>
							<p class="res-activity-time">
								<i class="fa fa-clock-o"></i>
								<?php
								    $actlog = strtotime($activitylog['logout_date_time']);
                                    echo $logtime = date(' h:i:s a',$actlog); 
								?>
							</p>
						</li>
						<?php
                            }
                          }   
						?>
					</ul>
					
						<ul class="timeline">	
                    <?php
                        $this->db->order_by('breakin', 'desc');
                        $this->db->limit(2);
                        $breaktime = $this->db->get_where('user_log', array('user_id' => $this->session->userdata('login_user_id'),'date'=>$curTime))->result_array();
                        
                        foreach($breaktime as $breaktime1)
                        {
                            if($breaktime1['breakin'] != '')
                            {
                    ?>
						<li>
							<p class="mb-0" style="margin:0px 0px 7px;color:black">Break In at</p>
							<p class="res-activity-time">
								<i class="fa fa-clock-o"></i>
								<?php
								    $brtime = $breaktime1['breakin'];
                                    echo $brtimes = date(' h:i:s a',strtotime($brtime)); 
								?>
							</p>
						</li>
						<?php
                            }
                            if($breaktime1['breakout'] != '')
                            {
                            ?>
                            	<li>
							<p class="mb-0" style="margin:0px 0px 7px;color:black">Break Out at</p>
							<p class="res-activity-time">
								<i class="fa fa-clock-o"></i>
								<?php
								    $brotime = $breaktime1['breakout'];
                                    echo $brotimes = date(' h:i:s a',strtotime($brotime)); 
								?>
							</p>
						</li>
                            
                            <?php
                            }
                          }   
						?>
					</ul>
                </div>
                
            </div>

<div class="col-md-12 col-xs-12" >
                    <div class="panel-heading">

                        <div class="panel-title">

                            <i class="fa fa-calendar"></i>

                            <?php echo get_phrase('event_schedule');?>

                        </div>

                    </div>

                    <div class="panel-body" style="padding:0px;">

                        <div class="calendar-env">

                            <div class="calendar-body" style="float: none;width: 100%;">

                                <div id="notice_calendar"></div>

                            </div>

                        </div>

                    </div>

                </div>


		</div>

    	

	</div>

</div>
<?php
    $m=1;
    $f=1;
    $sqlEmpl = $this->db->get_where('user')->result_array();
    foreach($sqlEmpl as $emplm)
    {
        
        if($emplm['gender']== 'male' )
        {
            $male = $m++;
        }
        else if($emplm['gender']== 'female')
        {
           $female = $f++;
        }
         
    }
?>
</div>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>

<script>
    /**
     * ---------------------------------------
     * This demo was created using amCharts 4.
     *
     * For more information visit:
     * https://www.amcharts.com/
     *
     * Documentation is available at:
     * https://www.amcharts.com/docs/v4/
     * ---------------------------------------
     */
    
    // Create chart instance
    var chart = am4core.create("chartdiv", am4charts.PieChart);
    
    // Add data
    chart.data = [{
      "icon": "male",
      "category": "male",
      "litres": <?php echo $male; ?>
    }, {
      "icon": "female",
      "category": "female",
      "litres": <?php echo $female; ?>
    },{
      "icon": "total-employees",
      "category": "total_employees",
      "litres": <?php echo $this->db->get_where('user', array('type' => 2))->num_rows(); ?>
      },{
      "icon": "on-leave",
      "category": "on-leave-employees",
      "litres": <?php echo $on_leave; ?>
    }];
    
    // Add and configure Series
    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "litres";
    pieSeries.dataFields.category = "category";
    pieSeries.labels.template.html = "<i class=\"material-icons\">{icon}</i>";
    pieSeries.alignLabels = false;
    
    chart.legend = new am4charts.Legend();
</script>
<style type="text/css">

#chartdiv,#chartdiv1 {
    width: 100%;
    height: 400px;
    border: 1px solid #ddd;
    box-shadow: 1px 1px 9px 4px #ddd;
    margin-bottom: 15px;
}

	.calendar-env .calendar-body .fc-header .fc-header-left {

		text-align: left;

	}



</style>



<script>

    $(document).ready(function() {

        var calendar = $('#notice_calendar');

        $('#notice_calendar').fullCalendar({

            header: {

                left: 'title',

                right: 'month,agendaWeek,agendaDay today prev,next'

            },

            

            //defaultView: 'basicWeek',

            

            editable: false,

            firstDay: 1,

            height: 530,

            droppable: false,

            

            events: [

                <?php 

                $notices = $this->db->get_where('noticeboard', array('status' => 1))->result_array();

                foreach($notices as $row): ?>

                {

                    title: "<?php echo $row['title'];?>",

                    start: new Date(<?php echo date('Y',$row['date']);?>, <?php echo date('m',$row['date'])-1;?>, <?php echo date('d',$row['date']);?>),

                    end:    new Date(<?php echo date('Y',$row['date']);?>, <?php echo date('m',$row['date'])-1;?>, <?php echo date('d',$row['date']);?>) 

                },

                <?php endforeach; ?>

            ]

        });

    });

</script>

<script>
       function printDiv() {
         window.frames["print_frame"].document.body.innerHTML = document.getElementById("printableTable").innerHTML;
         window.frames["print_frame"].window.focus();
         window.frames["print_frame"].window.print();
       }
</script>

<script>
 $(document).ready(function(){$("#myModal").modal("show")});
 
</script>

<style type="text/css">

.col-xs-12 { z-index:1; }

	.calendar-env .calendar-body .fc-header .fc-header-left {
		text-align: left;
	}
	ul.timeline {
    list-style-type: none;
    position: relative;
}
ul.timeline:before {
    content: ' ';
    background: #d4d9df;
    display: inline-block;
    position: absolute;
    left: 29px;
    width: 2px;
    height: 100%;
    z-index: 400;
}
ul.timeline > li {
    margin: 5px 0;
    padding-left: 20px;
}
ul.timeline > li:before {
    content: ' ';
    background: white;
    display: inline-block;
    position: absolute;
    border-radius: 50%;
    border: 3px solid #22c0e8;
    left: 25px;
    width: 10px;
    height: 10px;
    z-index: 400;
}
.punch-hours {
    align-items: center;
    background-color: #f9f9f9;
    /*border: 5px solid #e3e3e3;*/
    /*border-radius: 50%;*/
    display: flex;
    font-size: 18px;
        height: 119px;
    justify-content: center;
    /* margin: 0 auto; */
    margin-bottom: 0px;
    width: 0px;
}
</style>
  <style>
      @font-face {
  font-family: 'DIGITAL';
  src: url('https://cssdeck.com/uploads/resources/fonts/digii/DS-DIGII.TTF');
}


.digital-clock {
    align-items: center;
     justify-content: center;
    margin: auto;
    position: absolute;
    top: -38px;
    left: 0;
    bottom: 0;
    right: 0;
    width: 125px;
    height: 125px;
    color: #ffffff;
    border: 2px solid #999;
    border-radius: 50%;
    text-align: center;
    font: 23px/118px 'DIGITAL', Helvetica;
    background: linear-gradient(90deg, #000, #555);
}
  </style>          
  
<script>

    $(document).ready(function() {
  clockUpdate();
  setInterval(clockUpdate, 1000);
})

function clockUpdate() {
  var date = new Date();
  $('.digital-clock').css({'color': '#fff', 'text-shadow': '0 0 6px #ff0'});
  function addZero(x) {
    if (x < 10) {
      return x = '0' + x;
    } else {
      return x;
    }
  }

  function twelveHour(x) {
    if (x > 12) {
      return x = x - 12;
    } else if (x == 0) {
      return x = 12;
    } else {
      return x;
    }
  }

  var h = addZero(twelveHour(date.getHours()));
  var m = addZero(date.getMinutes());
  var s = addZero(date.getSeconds());

  $('.digital-clock').text(h + ':' + m + ':' + s)
}
</script>

<script language="javascript" type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
 
<script>
    
(function( $ ) {

    $.fn.thooClock = function(options) {
        
        this.each(function() {

        var cnv,
                ctx,
                el,
                defaults,
                settings,
                radius,
                dialColor,
                dialBackgroundColor,
                secondHandColor,
                minuteHandColor,
                hourHandColor,
                alarmHandColor,
                alarmHandTipColor,
                timeCorrection,
                x,
                y;

       defaults = {
                size: 250,
                dialColor: '#000000',
                dialBackgroundColor:'transparent',
                secondHandColor: '#F3A829',
                minuteHandColor: '#222222',
                hourHandColor: '#222222',
                alarmHandColor: '#FFFFFF',
                alarmHandTipColor: '#026729',
                timeCorrection: {
                    operator: '+',
                    hours: 0,
                    minutes: 0
                },
                alarmCount: 1,
                showNumerals: true,
                numerals: [
                    {1:1},
                    {2:2},
                    {3:3},
                    {4:4},
                    {5:5},
                    {6:6},
                    {7:7},
                    {8:8},
                    {9:9},
                    {10:10},
                    {11:11},
                    {12:12}
                ],
                sweepingMinutes: true,
                sweepingSeconds: false,
                numeralFont: 'arial',
                brandFont: 'arial'
            };

            settings = $.extend({}, defaults, options);

            el = this;

            el.size = settings.size;
            el.dialColor = settings.dialColor;
            el.dialBackgroundColor = settings.dialBackgroundColor;
            el.secondHandColor = settings.secondHandColor;
            el.minuteHandColor = settings.minuteHandColor;
            el.hourHandColor = settings.hourHandColor;
            el.alarmHandColor = settings.alarmHandColor;
            el.alarmHandTipColor = settings.alarmHandTipColor;
            el.timeCorrection = settings.timeCorrection;
            el.showNumerals = settings.showNumerals;
            el.numerals = settings.numerals;
            el.numeralFont = settings.numeralFont;

            el.brandText = settings.brandText;
            el.brandText2 = settings.brandText2;
            el.brandFont = settings.brandFont;
            
            el.alarmCount = settings.alarmCount;
            el.alarmTime = settings.alarmTime;
            el.onAlarm = settings.onAlarm;
            el.offAlarm = settings.offAlarm;

            el.onEverySecond = settings.onEverySecond;

            el.sweepingMinutes = settings.sweepingMinutes;
            el.sweepingSeconds = settings.sweepingSeconds;

            x=0; //loopCounter for Alarm
            
            cnv = document.createElement('canvas');
            ctx = cnv.getContext('2d');

            cnv.width = this.size;
            cnv.height = this.size;
            //append canvas to element
            $(cnv).appendTo(el);

            radius = parseInt(el.size/2, 10);
            //translate 0,0 to center of circle:
            ctx.translate(radius, radius); 

            //set alarmtime from outside:
            
            $.fn.thooClock.setAlarm = function(newtime){
                el.alarmTime = checkAlarmTime(newtime);
            };

            $.fn.thooClock.clearAlarm = function(){
                    el.alarmTime = undefined;
                    startClock(0,0);
                    $(el).trigger('offAlarm');
            };


            function checkAlarmTime(newtime){
                var thedate;
                if(newtime instanceof Date){
                    //keep date object
                    thedate=newtime;
                }
                else{
                    //convert from string formatted like hh[:mm[:ss]]]
                    var arr = newtime.split(':');
                    thedate=new Date();
                    for(var i= 0; i <3 ; i++){
                        //force to int
                        arr[i]=Math.floor(arr[i]);
                        //check if NaN or invalid min/sec
                        if( arr[i] !==arr[i] || arr[i] > 59) arr[i]=0 ;
                        //no more than 24h
                        if( i==0 && arr[i] > 23) arr[i]=0 ;
                    }
                    thedate.setHours(arr[0],arr[1],arr[2]);
                }
                //alert(el.id);
                return thedate;
            };
        

            function toRadians(deg){
                return ( Math.PI / 180 ) * deg;
            }     

            function drawDial(color, bgcolor){
                var dialRadius,
                    dialBackRadius,
                    i,
                    ang,
                    sang,
                    cang,
                    sx,
                    sy,
                    ex,
                    ey,
                    nx,
                    ny,
                    text,
                    textSize,
                    textWidth,
                    brandtextWidth,
                    brandtextWidth2;

                dialRadius = parseInt(radius-(el.size/50), 10);
                dialBackRadius = radius-(el.size/400);

                ctx.beginPath();
                ctx.arc(0,0,dialBackRadius,0,360,false);
                ctx.fillStyle = bgcolor;
                ctx.fill();
                 
                for (i=1; i<=60; i+=1) {
                    ang=Math.PI/30*i;
                    sang=Math.sin(ang);
                    cang=Math.cos(ang);
                    //hour marker/numeral
                    if (i % 5 === 0) {
                        ctx.lineWidth = parseInt(el.size/50,10);
                        sx = sang * (dialRadius - dialRadius/9);
                        sy = cang * -(dialRadius - dialRadius/9);
                        ex = sang * dialRadius;
                        ey = cang * - dialRadius;
                        nx = sang * (dialRadius - dialRadius/4.2);
                        ny = cang * -(dialRadius - dialRadius/4.2);
                        marker = i/5;

                        ctx.textBaseline = 'middle';
                        textSize = parseInt(el.size/13,10);
                        ctx.font = '100 ' + textSize + 'px ' + el.numeralFont;
                        ctx.beginPath();
                        ctx.fillStyle = color;

                        if(el.showNumerals && el.numerals.length > 0){
                            el.numerals.map(function(numeral){
                                if(marker == Object.keys(numeral)){
                                    textWidth = ctx.measureText (numeral[marker]).width;
                                    ctx.fillText(numeral[marker],nx-(textWidth/2),ny);
                                }
                            });
                        }
                    //minute marker
                    } else {
                       ctx.lineWidth = parseInt(el.size/100,10);
                        sx = sang * (dialRadius - dialRadius/20);
                        sy = cang * -(dialRadius - dialRadius/20);
                        ex = sang * dialRadius;
                        ey = cang * - dialRadius;
                    }

                    ctx.beginPath();
                    ctx.strokeStyle = color;
                    ctx.lineCap = "round";
                    ctx.moveTo(sx,sy);
                    ctx.lineTo(ex,ey);
                    ctx.stroke();
                } 

                if(el.brandText !== undefined){
                    ctx.font = '100 ' + parseInt(el.size/28,10) + 'px ' + el.brandFont;
                    brandtextWidth = ctx.measureText (el.brandText).width;
                    ctx.fillText(el.brandText,-(brandtextWidth/2),(el.size/6)); 
                }

                if(el.brandText2 !== undefined){
                    ctx.textBaseline = 'middle';
                    ctx.font = '100 ' + parseInt(el.size/44,10) + 'px ' + el.brandFont;
                    brandtextWidth2 = ctx.measureText (el.brandText2).width;
                    ctx.fillText(el.brandText2,-(brandtextWidth2/2),(el.size/5)); 
                }

            }

            function twelvebased(hour){
                if(hour >= 12){
                    hour = hour - 12;
                }
                return hour;
            }

            function drawHand(length){
               ctx.beginPath();
               ctx.moveTo(0,0);
               ctx.lineTo(0, length * -1);
               ctx.stroke();
            }
            
            function drawSecondHand(milliseconds, seconds, color){
                var shlength = (radius)-(el.size/40);
                
                ctx.save();
                ctx.lineWidth = parseInt(el.size/150,10);
                ctx.lineCap = "round";
                ctx.strokeStyle = color;

                ctx.rotate( toRadians((milliseconds * 0.006) + (seconds * 6)));

                ctx.shadowColor = 'rgba(0,0,0,.5)';
                ctx.shadowBlur = parseInt(el.size/80,10);
                ctx.shadowOffsetX = parseInt(el.size/200,10);
                ctx.shadowOffsetY = parseInt(el.size/200,10);

                drawHand(shlength);

                //tail of secondhand
                ctx.beginPath();
                ctx.moveTo(0,0);
                ctx.lineTo(0, shlength/15);
                ctx.lineWidth = parseInt(el.size/30,10);
                ctx.stroke();

                //round center
                ctx.beginPath();
                ctx.arc(0, 0, parseInt(el.size/30,10), 0, 360, false);
                ctx.fillStyle = color;

                ctx.fill();
                ctx.restore();
            }

            function drawMinuteHand(minutes, color){
                var mhlength = el.size/2.2;
                ctx.save();
                ctx.lineWidth = parseInt(el.size/50,10);
                ctx.lineCap = "round";
                ctx.strokeStyle = color;
               
                if(!el.sweepingMinutes){
                    minutes.isInteger ? minutes : minutes = parseInt(minutes);
                }
                ctx.rotate( toRadians(minutes * 6));

                ctx.shadowColor = 'rgba(0,0,0,.5)';
                ctx.shadowBlur = parseInt(el.size/50,10);
                ctx.shadowOffsetX = parseInt(el.size/250,10);
                ctx.shadowOffsetY = parseInt(el.size/250,10);

                drawHand(mhlength);
                ctx.restore();
            }

            function drawHourHand(hours, color){
                var hhlength = el.size/3;
                ctx.save();
                ctx.lineWidth = parseInt(el.size/25, 10);
                ctx.lineCap = "round";
                ctx.strokeStyle = color;
                ctx.rotate( toRadians(hours * 30));

                ctx.shadowColor = 'rgba(0,0,0,.5)';
                ctx.shadowBlur = parseInt(el.size/50, 10);
                ctx.shadowOffsetX = parseInt(el.size/300, 10);
                ctx.shadowOffsetY = parseInt(el.size/300, 10);

                drawHand(hhlength);
                ctx.restore();
            }

            function timeToDecimal(time){
                var h,
                    m;
                if(time !== undefined){
                    h = twelvebased(time.getHours());
                    m = time.getMinutes();
                }
                return parseInt(h,10) + (m/60);
            }

            function drawAlarmHand(alarm, color, tipcolor){

                var ahlength = el.size/2.4;
                
                ctx.save();
                ctx.lineWidth = parseInt(el.size/30, 10);
                ctx.lineCap = "butt";
                ctx.strokeStyle = color;

                //decimal equivalent to hh:mm
                alarm = timeToDecimal(alarm);
                ctx.rotate( toRadians(alarm * 30));

                ctx.shadowColor = 'rgba(0,0,0,.5)';
                ctx.shadowBlur = parseInt(el.size/55, 10);
                ctx.shadowOffsetX = parseInt(el.size/300, 10);
                ctx.shadowOffsetY = parseInt(el.size/300, 10);  

                ctx.beginPath();
                ctx.moveTo(0,0);
                ctx.lineTo(0, (ahlength-(el.size/10)) * -1);
                ctx.stroke();

                ctx.beginPath();
                ctx.strokeStyle = tipcolor;
                ctx.moveTo(0, (ahlength-(el.size/10)) * -1);
                ctx.lineTo(0, (ahlength) * -1);
                ctx.stroke();

                //round center
                ctx.beginPath();
                ctx.arc(0, 0, parseInt(el.size/24, 10), 0, 360, false);
                ctx.fillStyle = color;
                ctx.fill();
                ctx.restore();
            }  

            //listener
            if(el.onAlarm !== undefined){
            	$(el).on('onAlarm', function(e){
                	el.onAlarm();
                	e.preventDefault();
                	e.stopPropagation();
            	});
            }

            if(el.onEverySecond !== undefined){
                $(el).on('onEverySecond', function(e){
                    el.onEverySecond();
                    e.preventDefault();
                });
            }

            if(el.offAlarm !== undefined){
	            $(el).on('offAlarm', function(e){
    	            el.offAlarm();
        	        e.stopPropagation();
            	    e.preventDefault();
           		});
			}

            y=0;

            function startClock(x){
                var theDate,
                    ms,
                    s,
                    m,
                    hours,
                    mins,
                    h,
                    exth,
                    extm,
                    allExtM,
                    allAlarmM,
                    atime;

                theDate = new Date();
                //  alert(theDate);
                if(el.timeCorrection){
                    if(el.timeCorrection.operator === '+'){
                        theDate.setHours(theDate.getHours() + el.timeCorrection.hours);
                        theDate.setMinutes(theDate.getMinutes() + el.timeCorrection.minutes);
                    }
                    if(el.timeCorrection.operator === '-'){
                        theDate.setHours(theDate.getHours() - el.timeCorrection.hours);
                        theDate.setMinutes(theDate.getMinutes() - el.timeCorrection.minutes);
                    }
                }

    
                // s = theDate.getSeconds();
                s = <?php echo date('s',$an);?>;
                el.sweepingSeconds ? ms = theDate.getMilliseconds() : ms = 0;
                // mins = theDate.getMinutes();
                mins = <?php echo date('i',$an);?>;
                // alert(mins);
                // m = (mins  + (s/60));
                m = <?php echo date('m',$an);?>;
                // alert(m);
                // hours = theDate.getHours();
                hours = <?php echo date('h',$an);?>;
                // alert(hours);
                h = twelvebased(hours + (m/60));
                //h =<?php echo date('h',$an);?>;
                //alert(h);
                ctx.clearRect(-radius,-radius,el.size,el.size);

                drawDial(el.dialColor, el.dialBackgroundColor);

                if(el.alarmTime !== undefined){
                    el.alarmTime = checkAlarmTime(el.alarmTime);
                    drawAlarmHand(el.alarmTime, el.alarmHandColor, el.alarmHandTipColor);
                }
                drawHourHand(h, el.hourHandColor);
                drawMinuteHand(m, el.minuteHandColor);
                drawSecondHand(ms, s, el.secondHandColor);

                //trigger every second custom event
                if(y !== s){
                    $(el).trigger('onEverySecond');
                    y = s;
                }
               
                if(el.alarmTime !== undefined){
                    allExtM = (el.alarmTime.getHours()*60*60) + (el.alarmTime.getMinutes() *60) + el.alarmTime.getSeconds();
                }

                allAlarmM = (hours*60*60) + (mins*60) + s;

                //alarmMinutes greater than passed Minutes;
                if(allAlarmM >= allExtM){
                    x+=1; 
                }
                //trigger alarm for as many times as i < alarmCount
                if(x <= el.alarmCount && x !== 0){
                   $(el).trigger('onAlarm');
                }
                
                window.requestAnimationFrame(function(){startClock(x)});

            }

            startClock(x);

   });//return each this;
  };     

}(jQuery));
</script>