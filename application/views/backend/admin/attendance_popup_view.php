<?php


$curTime = date('Y-m-d',time());
$date = strtotime(date("d-m-Y"));


$view_data = $this->db->get_where('attendance', array('date'=>$date))->result_array();
    
 ?>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title" >
                        <?php //echo get_phrase("user_log")."- ( ".$curTime.") "; ?>
                    </div>
                </div>

                <div class="panel-body">
                    
                    <table class="table table-dark">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <!-- <th scope="col">Login Time</th>
                          <th scope="col">Logout</th> -->
                        </tr>
                      </thead>
                      <tbody>
                          <?php 
                         
                          $c=1;
                        foreach($view_data as $row) {

                         $userId = $row['user_id'];

                          $view_data2 = $this->db->get_where('user', array('user_id'=>$userId))->row();
                           // echo "<b>Login Time : </b>".$row['created_at']."<b>Logout Time : </b>".$row['logout_date_time'];
                            
                         ?>
                        <tr>
                          <th scope="row"><?php echo $c++;?></th>
                          <th scope="row"><?php echo $view_data2->name;?></th>
                         <!--  <td><?php //echo $row['created_at']; ?></td> -->
                         <!--  <td><?php 
                          //echo "ll".$row['logout_date_time'];
                           // if($row['logout_date_time'] == '')
                            {
                               // echo "";
                            }
                            //else
                            {
                               // echo date('h:i:s',strtotime($row['logout_date_time']));
                                
                            }
                          ?></td> -->
                        </tr>
                        <?php } ?>
                       
                      </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


<script type="text/javascript">

    $( document ).ready(function() {

        // SelectBoxIt Dropdown replacement
        if($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function(i, el)
            {
                var $this = $(el),
                    opts = {
                        showFirstOption: attrDefault($this, 'first-option', true),
                        'native': attrDefault($this, 'native', false),
                        defaultText: attrDefault($this, 'text', ''),
                    };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>
