<?php 
$edit_data = $this->db->get_where('support', array('token' => $param2))->result_array();

foreach($edit_data as $row) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class=""></i>
                    <?php echo get_phrase('add_support'); ?>
                </div>
            </div>

            <div class="panel-body">

                <?php echo form_open(site_url('admin/support/complete'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('send_request_to*'); ?></label>

                    <div class="col-sm-5">
                        <select  class="form-control" name = "report_status" required>
                             <option value = "">*Select Report Status</option>
                             <option value = "complete">complete</option>
                             <option value = "pending">pending</option>
                             <option value = "cancel">cancel</option>
                        </select>
                    </div>
                </div>
                

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>
                    <input type = "hidden" name = "token" value = "<?php echo $row['token'];?>">
                    <div class="col-sm-8">
                        <textarea class="form-control" name="report_description" rows="3" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('submit'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
