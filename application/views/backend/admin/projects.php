<a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/project_add'); ?>');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_new_project'); ?>
</a> 
<br><br><br>

<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div><?php echo get_phrase('project_code'); ?></div></th>
            <th><div><?php echo get_phrase('project_name'); ?></div></th>
            <th><div><?php echo get_phrase('priority'); ?></div></th>
            <th><div><?php echo get_phrase('document'); ?></div></th>
            <th><div><?php echo get_phrase('start_date'); ?></div></th>
            <th><div><?php echo get_phrase('end_date'); ?></div></th>
            <th><div><?php echo get_phrase('Status'); ?></div></th>
            <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        $this->db->order_by('id', 'asc');
        $projects = $this->db->get('projects')->result_array();
        foreach ($projects as $row):
            ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $row['project_code']; ?></td>
                <td><?php echo $row['project_name']; ?></td>
                <td><?php echo $row['priority']; ?></td>
                <td><?php if($row['document'] !=''){ ?><a href = "<?php echo base_url().$row['document']; ?>" download> Download <i class="fa fa-download" aria-hidden="true"></i></a><?php }else{ echo "No Document"; } ?></td>
                <td><?php echo date('yy-m-d',$row['start_date']); ?></td>
                <td><?php echo date('yy-m-d', $row['end_date']); ?></td>
                <td><?php echo $row['project_status']; ?></td>
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                             <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/project_team/'.$row['project_code']); ?>');">
                                   <i class="fa fa-eye" aria-hidden="true"></i>
                                <?php echo get_phrase('team'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/project_edit/'.$row['id']); ?>');">
                                    <i class="entypo-pencil"></i>
                                <?php echo get_phrase('update'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <li>
                                <a href="#" onclick="confirm_modal_hard_reload('<?php echo site_url('admin/projects/delete/'.$row['id'] ); ?>');">
                                    <i class="entypo-trash"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>
            
    <?php endforeach; ?>
    </tbody>
</table>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(7, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>