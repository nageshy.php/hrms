<!--<a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/support_create'); ?>');" -->
<!--   class="btn btn-primary pull-right">-->
<!--    <i class="entypo-plus-circled"></i>-->
<!--    <?php //echo get_phrase('add_new_support'); ?>-->
<!--</a> -->
<br><br><br>

<table class="table table-bordered">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div>ID</div></th>
            <th><div><?php echo get_phrase('requested_from'); ?></div></th>
             <th><div><?php echo get_phrase('support_type'); ?></div></th>
            <th><div><?php echo get_phrase('description'); ?></div></th>
            <th><div><?php echo get_phrase('status'); ?></div></th>
            <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        
        //$uid = $this->session->userdata('login_user_id');
        $userResult = $this->db->get_where('user', array('user_id' => $this->session->userdata('login_user_id')))->row();
        $uname = $userResult->name;
        
        //$this->db->order_by('id', 'desc');
        $support = $this->db->get_where('support',
            array('request_to' =>$uname ))->result_array();
        foreach($support as $row): ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $row['token']; ?></td>
                <td>
                    <?php 
                    
                        $userId = $row['user_id']; 
                        $empName = $this->db->get_where('user',array('user_id'=>$userId))->row();
                        echo $empName->name;
                    ?>
                </td>
                <td><?php echo $row['request_type']; ?></td>
                <td><?php echo substr($row['description'], 0, 50) . '...'; ?></td>
               <td>
                    <?php
                    if($row['status'] == 0)
                        echo '<div class="label label-info">' . get_phrase('pending') . '</div>';
                    if($row['status'] == 1)
                        echo '<div class="label label-success">' . get_phrase('compelted') . '</div>';
                    if($row['status'] == 2)
                        echo '<div class="label label-danger">' . get_phrase('declined') . '</div>';
                    ?>
                </td>
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/support_complete/'.$row['token']); ?>');">
                                    <i class="entypo-pencil"></i>
                                <?php echo get_phrase('respond'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <li>
                                <a href="#" onclick="confirm_modal_hard_reload('<?php echo site_url('admin/support/delete/'.$row['token']); ?>');">
                                    <i class="entypo-trash"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>
    <?php endforeach; ?>
    </tbody>
</table>