<?php
$logged_in_user = $this->db->get_where('user', array('user_id' => $this->session->userdata('login_user_id')))->row();
 $userId = $this->session->userdata('login_user_id');


?>

<div class="row">
    <div class="col-md-12 col-sm-12 clearfix" style="text-align:center;">
        <h2 style="margin:0px;font-family: -webkit-body;font-size: xxx-large;font-weight: 900;font-variant-caps: all-small-caps;font-weight: lighter;border-bottom: 3px solid #706fd7;box-shadow: 1px 1px 20px 4px #ddd;">
            <?php echo $system_name; ?>
        </h2>
    </div>
    <!-- Raw Links -->
    <div class="col-md-12 col-sm-12 clearfix ">

        <ul class="list-inline links-list pull-left">
            <!-- Language Selector -->			
            <li class="dropdown language-selector">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-close-others="true">
                    <i class="entypo-user"></i>
                    <?php echo $logged_in_user->name . ' (' . get_phrase($logged_in_user_type) . ')'; ?>
                    <i class="fa fa-caret-down" aria-hidden="true"></i>

                </a>

                <ul class="dropdown-menu pull-left">
                    <?php
                    if($logged_in_user_type == 'admin') {
                        $edit_profile_link      = 'admin/manage_profile';
                        $change_password_link   = 'admin/manage_profile';
                    } else {
                        $edit_profile_link      = 'employee/profile';
                        $change_password_link   = 'employee/change_password';
                    }
                    ?>
                    <li>
                        <a href="<?php echo site_url($edit_profile_link); ?>">
                            <i class="entypo-info"></i>
                            <span><?php echo get_phrase('edit_profile'); ?></span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url($change_password_link); ?>">
                            <i class="entypo-key"></i>
                            <span><?php echo get_phrase('change_password'); ?></span>
                        </a>
                    </li>
                </ul>
                
            </li>
            
<!--              <li class="notifications dropdown">-->
<!--   <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" aria-expanded="false"> <i class="entypo-mail"></i> <span class="badge badge-secondary">10</span> </a> -->
<!--   <ul class="dropdown-menu">-->
      <!-- TS16052498834697: Xenon - Boostrap Admin Template created by Laborator / Please buy this theme and support the updates --> 
<!--      <li>-->
<!--         <form class="top-dropdown-search">-->
<!--            <div class="form-group"> <input type="text" class="form-control" placeholder="Search anything..." name="s"> </div>-->
<!--         </form>-->
<!--         <ul class="dropdown-menu-list scroller" tabindex="5002" style="overflow: hidden; outline: none;">-->
<!--            <li class="active"> <a href="#"> <span class="image pull-right"> <img src="https://demo.neontheme.com/assets/images/thumb-1@2x.png" width="44" alt="" class="img-circle"> </span> <span class="line"> <strong>Luc Chartier</strong>-->
<!--               - yesterday-->
<!--               </span> <span class="line desc small">-->
<!--               This ain’t our first item, it is the best of the rest.-->
<!--               </span> </a> -->
<!--            </li>-->
<!--         </ul>-->
<!--      </li>-->
<!--      <li class="external"> <a href="#">All Messages</a> </li>-->
<!--      <div id="ascrail2002" class="nicescroll-rails" style="padding-right: 3px; width: 10px; z-index: 1000; cursor: default; position: absolute; top: 0px; left: -10px; height: 0px; display: none;">-->
<!--         <div style="position: relative; top: 0px; float: right; width: 5px; height: 0px; background-color: rgb(212, 212, 212); border: 1px solid rgb(204, 204, 204); background-clip: padding-box; border-radius: 1px;"></div>-->
<!--      </div>-->
<!--      <div id="ascrail2002-hr" class="nicescroll-rails" style="height: 7px; z-index: 1000; top: -7px; left: 0px; position: absolute; cursor: default; display: none;">-->
<!--         <div style="position: relative; top: 0px; height: 5px; width: 0px; background-color: rgb(212, 212, 212); border: 1px solid rgb(204, 204, 204); background-clip: padding-box; border-radius: 1px;"></div>-->
<!--      </div>-->
<!--   </ul>-->
<!--</li>-->
        </ul>


        <ul class="list-inline links-list pull-right">
          
            <li> <a href="<?php echo site_url('admin/message'); ?>"> <i class="entypo-chat"></i>
                Chat
                <?php
                    $i=1;
                    if ($this->session->userdata('login_type') == 'employee') {
                    $messageF = $this->db->get_where('message',array('read_status'=>2))->result_array();
                        foreach($messageF as $messNotify)
                        {
                            $senderId = $messNotify['sender'];
                            $senderId = str_replace( '-', '', $senderId );
                           $senderId = (int) filter_var($senderId, FILTER_SANITIZE_NUMBER_INT);
                          $logged_in_user->user_id;
                            if($senderId == $logged_in_user->user_id)
                            {
                               //echo $messNotify['read_status'] . "<br/>";
                                // if($messNotify['read_status'] == '' && $messNotify['message_thread_code'])
                                // {
                                //   $messNotify1 = $messNotify['message'];
                                    ?>
                                    
                                    <span class='badge badge-success chat-notifications-badge'>
                                    <?php } //echo $i++ ; ?>
                                    
                                    </span> </a> 
                <?php 
                                // }
                            }
                           
                        }
                ?>
                
                <?php
                if ($this->session->userdata('login_type') == 'admin') {
                    $i1=1;
                    $messageF = $this->db->get_where('message_thread',array('status'=>2))->result_array();
                        foreach($messageF as $messNotify)
                        {
                            $senderId = $messNotify['sender'];
                            $senderId = str_replace( '-', '', $senderId );
                           $senderId = (int) filter_var($senderId, FILTER_SANITIZE_NUMBER_INT);
                          $logged_in_user->user_id;
                            if($senderId == $logged_in_user->user_id)
                            {
                               //echo $messNotify['read_status'] . "<br/>";
                                // if($messNotify['read_status'] == '' && $messNotify['message_thread_code'])
                                // {
                                //   $messNotify1 = $messNotify['message'];
                                    ?>
                                    
                                    <span class='badge badge-success chat-notifications-badge'>
                                    <?php echo $i1++; } ?>
                                    
                                    </span> </a> 
                <?php 
                                // }
                            }
                           
                        }
                ?>
                </li>
            <li>
                <a href="<?php echo site_url('login/logout'); ?>">
                    <?php echo get_phrase('log_out'); ?>
                    <i class="entypo-logout right"></i>
                </a>
            </li>
        </ul>
    </div>

</div>
<hr style="margin-top:0px;" />