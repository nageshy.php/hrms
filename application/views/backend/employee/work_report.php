<?php
    //ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL);
?>
<a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/work_report_add'); ?>');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_work_report'); ?>
</a> 
<br><br><br>
<div class="table-responsive">
<table class="table table-bordered datatable" id="table_export">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div><?php echo get_phrase('project_name'); ?></div></th>
            <th><div><?php echo get_phrase('module'); ?></div></th>
            <!--<th><div><?php echo get_phrase('work_type'); ?></div></th>-->
            <th><div><?php echo get_phrase('work_date'); ?></div></th>
            <th><div><?php echo get_phrase('duration'); ?></div></th>
            <th><div><?php echo get_phrase('start_time'); ?></div></th>
            <th><div><?php echo get_phrase('end_time'); ?></div></th>
            <th><div><?php echo get_phrase('details'); ?></div></th>
             <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        $this->db->order_by('id', 'asc');
        $reports = $this->db->get_where('work_report',array('user_id'=>$this->session->userdata('login_user_id')))->result_array();
        
        foreach ($reports as $row):
            ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php 
                 $project = $this->db->get_where('projects',array('project_code'=>$row['project_code']))->result_array();
                foreach($project as $projectname)
                {
                    echo $projectname['project_name'];
                }
                ?>
                </td>
                <td><?php echo $row['project_module']; ?></td>
                <!--<td><?php //echo $row['work_types']; ?></td>-->
                <td><?php echo $row['work_date']; ?></td>
                <td><?php echo $row['duration']; ?></td>
                <td><?php echo $row['start_time']; ?></td>
                <td><?php echo $row['end_time']; ?></td>
                <td><?php echo $row['details']; ?></td>
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/work_report_edit/'.$row['id']); ?>');">
                                    <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <li>
                                <a  onclick="confirm_modal_hard_reload('<?php echo site_url('employee/work_report/delete/'.$row['id'] ); ?>');">
                                    <i class="entypo-trash"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>
            
    <?php endforeach; ?>
    </tbody>
</table>
</div>

<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function ($)
    {
        var datatable = $("#table_export").dataTable({
            "sPaginationType": "bootstrap",
            "sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
            "oTableTools": {
                "aButtons": [
                    {
                        "sExtends": "xls",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "pdf",
                        "mColumns": [1, 2, 3, 4, 5, 6]
                    },
                    {
                        "sExtends": "print",
                        "fnSetText": "Press 'esc' to return",
                        "fnClick": function (nButton, oConfig) {
                            datatable.fnSetColumnVis(7, false);

                            this.fnPrint(true, oConfig);

                            window.print();

                            $(window).keyup(function (e) {
                                if (e.which == 27) {
                                    datatable.fnSetColumnVis(3, true);
                                }
                            });
                        },
                    },
                ]
            },
        });

        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });

</script>