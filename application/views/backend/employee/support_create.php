<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class=""></i>
                    <?php echo get_phrase('add_support'); ?>
                </div>
            </div>

            <div class="panel-body">

                <?php echo form_open(site_url('employee/support/create'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('send_request_to*'); ?></label>

                    <div class="col-sm-5">
                        <select  class="form-control" name = "request_to" required>
                             <option>*Select Send Request To</option>
                             <?php
                                $support = $this->db->get_where('user',array('type'=>1))->result_array();
                                foreach($support as $suporterName)
                                {
                                                                        
                                
                             ?>
                            <option value = "<?php echo $suporterName['name']; ?>"><?php echo $suporterName['name'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('request_type*'); ?></label>

                    <div class="col-sm-5">
                       <select class="form-control" name = "request_type" id="request" >
                        <option value = "">*Select Request Type</option>
                        <option value = "basic format">Basic Format</option>
                        <option value = "request for information">Request for Information</option>
                      </select>  
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('subject'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="subject" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>

                    <div class="col-sm-8">
                        <textarea class="form-control" name="description" rows="3" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('submit'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

