<?php
$edit_data = $this->db->get_where('work_report', array('id' => $param2))->result_array();
foreach($edit_data as $row) { ?>
                <div class="modal-body" style="height:500px; overflow:auto; width: 100%;">
               
                <?php echo form_open(site_url('employee/work_report/edit'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="project"><?php echo get_phrase('projects'); ?></label>
                                    <?php
                                        $projects = $this->db->get_where('projects',array('status'=>1))->result_array();
                                        
                                    ?>
                                    <select class="form-control" name = "project_code" id="project" >
                                        <option value = ""><?php echo get_phrase('projects'); ?></option>
                                        <?php 
                                            foreach($projects as $projectName)
                                            {
                                        ?>
                                      <option value = '<?php echo $projectName['project_code']; ?>' ><?php echo $projectName['project_name']; ?></option>
                                      <?php } ?>
                                    </select>                          
                        </div>
                            </div>
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="modules"><?php echo get_phrase('project_module'); ?></label>
                       <input type = 'hidden' name = 'report_code' value = "<?php echo $row['report_code'];?>" />
                                <input type = 'text' class="form-control" id="modules" name = 'project_module' value = "<?php echo $row['project_module'];?>" placeholder = 'Project Module ' />
                    </div>
                </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">  
                                
                                <div class="form-group">
                                    <label for="url types"><?php echo get_phrase('url_type'); ?></label>
                                    <select class="form-control" name = "url_type" id="url types" >
                                      <option value = '<?php echo $row['url_type'];?>' ><?php echo $row['url_type'];?></option>
                                      <option value = 'general' >General</option>
                                      <option value = 'fully qualified' >Fully Qualified </option>
                                      <option value = 'from the root' >From the Root </option>
                                     <option value = 'relative' > Relative </option>
                                    </select>                          
                        </div>
                            </div>
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="types"><?php echo get_phrase('work_types'); ?></label>
                                <select class="form-control" name = 'work_types' id="types">
                                <option  value = '<?php echo $row['work_types'];?>' ><?php echo $row['work_types'];?></option>
                                  <option value = 'work from home' >Work From Home</option>
                                      <option value = 'work from office' >Work From Office</option>                        
                                  
                                </select>
                    </div>
                </div>
                        </div>
                        <div class="row">
                         
                      
                        <div class="col-md-6">   
                            <div class="form-group">
                                <label for="workdate"><?php echo get_phrase('work_date'); ?></label>
                                <input type="date" name = 'work_date' id="workdate" value = "<?php echo $row['work_date'];?>" class="form-control">
                    </div>
                </div>
                        
                            <div class="col-md-6">  
                                 <div class="form-group">
                                    <label for="duration"><?php echo get_phrase('duration'); ?></label>
                                   <input type="time" name = "duration" id="duration" value = "<?php echo $row['duration'];?>"  class="form-control">                        
                                 </div>
                            </div>
                      
                      
                        </div>
                        
                         <div class="row">
                         
                      
                        <div class="col-md-6">  
                                 <div class="form-group">
                                    <label for="duration"><?php echo get_phrase('start_time'); ?></label>
                                   <input type="time" id="duration" name = "start_time"  value = "<?php echo $row['start_time'];?>"  class="form-control">                        
                                 </div>
                            </div>
                        
                            <div class="col-md-6">  
                                 <div class="form-group">
                                    <label for="duration"><?php echo get_phrase('end_time'); ?></label>
                                   <input type="time" id="duration" name = "end_time"  value = "<?php echo $row['end_time'];?>"  class="form-control">                        
                                 </div>
                            </div>
                      
                      
                        </div>
                        
                        <div class="row">
                            <div class="col-md-12">  
                                
                                <div class="form-group">
                                    <label for="Details">Details</label>
                                  <textarea name="details" id="Details" cols="" rows="5" class="form-control" placeholder="Details">  <?php echo $row['details'];?></textarea>                      
                        </div>
                            </div>
                      
                      
                        </div>
                        <div class="form-group">
                    <div class="col-sm-offset-5 col-sm-12">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('submit'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
                    
                </div>
<?php } ?>
                