<?php 
$edit_data = $this->db->get_where('support', array('token' => $param2))->result_array();

foreach($edit_data as $row) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" >
                    <i class=""></i>
                    <?php echo get_phrase('add_support'); ?>
                </div>
            </div>

            <div class="panel-body">

                <?php echo form_open(site_url('employee/support/edit'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('send_request_to*'); ?></label>

                    <div class="col-sm-5">
                        <select  class="form-control" name = "request_to" required>
                             <option><?php echo $row['request_to'];?></option>
                             <?php
                                $support = $this->db->get_where('user',array('type'=>1))->result_array();
                                foreach($support as $suporterName)
                                {
                                                                        
                                
                             ?>
                            <option value = "<?php echo $suporterName['user_id']; ?>"><?php echo $suporterName['name'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('request_type*'); ?></label>

                    <div class="col-sm-5">
                       <select class="form-control" name = "request_type" id="request" >
                        <option value = ""><?php echo $row['request_type'];?></option>
                        <option value = "basic format">Basic Format</option>
                        <option value = "request for information">Request for Information</option>
                      </select>  
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo get_phrase('subject'); ?></label>

                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="subject" value = "<?php echo $row['subject'];?>" required />
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('description'); ?></label>
                    <input type = "hidden" name = "token" value = "<?php echo $row['token'];?>">
                    <div class="col-sm-8">
                        <textarea class="form-control" name="description" rows="3" required><?php echo $row['description'];?></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info"><?php echo get_phrase('submit'); ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>
