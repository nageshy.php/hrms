<a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/support_create'); ?>');" 
   class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    <?php echo get_phrase('add_new_support'); ?>
</a> 
<br><br><br>
<div class="table-responsive">
<table class="table table-bordered">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div>ID</div></th>
            <th><div><?php echo get_phrase('requested_to'); ?></div></th>
             <th><div><?php echo get_phrase('support_type'); ?></div></th>
            <th><div><?php echo get_phrase('description'); ?></div></th>
            <th><div><?php echo get_phrase('reported_by'); ?></div></th>
            <th><div><?php echo get_phrase('status'); ?></div></th>
            <th><div><?php echo get_phrase('options'); ?></div></th>
        </tr>
    </thead>
    <tbody>
        <?php
        $count = 1;
        
        //$this->db->order_by('id', 'desc');
        $support = $this->db->get_where('support',
            array('user_id' => $this->session->userdata('login_user_id')))->result_array();
        foreach($support as $row): ?>
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo $row['token']; ?></td>
                <td>
                    <?php 
                    
                        echo $userId = $row['request_to']; 
                    ?>
                </td>
                <td><?php echo $row['request_type']; ?></td>
                <td><?php echo substr($row['description'], 0, 50) . '...'; ?></td>
                <td><?php  if($row['reported_by'] == ""){ echo "<div class='label label-info'>Pending</div>"; }else{ echo $row['reported_by']; } ?></td>                <td>
                    <?php
                    if($row['status'] == 0)
                        echo '<div class="label label-info">' . get_phrase('pending') . '</div>';
                    if($row['status'] == 1)
                        echo '<div class="label label-success">' . get_phrase('compelted') . '</div>';
                    if($row['status'] == 2)
                        echo '<div class="label label-danger">' . get_phrase('declined') . '</div>';
                    ?>
                </td>
                <td>

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                            <li>
                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/support_edit/'.$row['token']); ?>');">
                                    <i class="entypo-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                                </a>
                            </li>
                            <li class="divider"></li>

                            <li>
                                <a href="#" onclick="confirm_modal_hard_reload('<?php echo site_url('employee/support/delete/'.$row['token']); ?>');">
                                    <i class="entypo-trash"></i>
                                    <?php echo get_phrase('delete'); ?>
                                </a>
                            </li>
                        </ul>
                    </div>

                </td>
            </tr>
    <?php endforeach; ?>
    </tbody>
</table>
</div>