<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="SVSK TECHNOLOGIES Admin Panel" />
	<meta name="author" content="" />

	<link rel="icon" href="<?php echo base_url();?>uploads/favicon.png">

	<title><?php echo $page_title;?> | <?php echo $system_name;?></title>

	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-icons/entypo/css/entypo.css">
	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-core.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-theme.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-forms.css">
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">
  	<link rel="stylesheet" href="<?php echo base_url();?>assets/js/datatables/responsive/css/datatables.responsive.css">

<!--  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
 --> 	<script src="<?php echo base_url();?>assets/js/jquery-1.11.3.min.js"></script>
<!--<script src="<?php //echo base_url();?>assets/js/jquery.thooClock.js"></script>-->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
 -->
	<!-- AMCHARTS -->
	<script src="assets/amcharts/amcharts.js"></script>
	<script src="assets/amcharts/serial.js"></script>
	<script src="assets/amcharts/pie.js"></script>
	<script src="assets/amcharts/themes/light.js"></script>
	<script src="assets/amcharts/plugins/export/export.js"></script>
	<script src="assets/amcharts/plugins/export/export.css"></script>
	<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-137838672-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-137838672-1');
		</script>
</head>