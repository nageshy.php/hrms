<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*

 *  @author     : Y Nagesh,SVSK TEAM

 *  date        : 10 july, 2020

 *  Human Resource Management System
    Nagesh.y@svsktechnologies.com
 */


class Email_model extends CI_Model {



	function __construct()

    {

        parent::__construct();

         $this->load->library('email');   

    }



    function account_opening_email($email = '', $password = '')

	{

		$system_name	=	$this->db->get_where('settings' , array('type' => 'system_name'))->row()->description;



		$email_msg		=	"<h2>Welcome to ".$system_name."</h2><br />";
		
		$email_msg		.=	"<b>Your login Id : </b>". $email ."<br />";

		$email_msg		.=	"<b>Your login password : </b>". $password ."<br />";

		$email_msg		.=	"Login Here : <a href = '".base_url()."'><button  style='background: #2f63a0;color: #fff;border-radius: 10px;padding: 10px; border: 1px solid #fff;'>PLEASE LOGIN </button></a><br />";
		
		$email_msg		.=	"<br /><br /><br />Best Regards, <br />D Keerthi,<br/>Hr Manager<br/><br/><img src = 'http://svsktechnologies.com/wp-content/uploads/2019/12/SVSK-PNG-1536x613.png' width = '150px'>";
            
        // echo $email_msg;
        // die;


		$email_sub		=	"Employee Account opening email";

		$email_to		=	$email;



		$this->do_email($email_msg , $email_sub , $email_to);
		
		    $header = "From:svsktechnologies20@gmail.com \r\n";

             $header .= "Cc:svsktechnologies20@gmail.com \r\n";

             $header .= "MIME-Version: 1.0\r\n";

             $header .= "Content-type: text/html\r\n";

			$mailSent = mail($email_to,$email_sub,$email_msg,$header);

	}



	function password_reset_email($new_password = '' , $email = '') {



		$query			=	$this->db->get_where('user' , array('email' => $email));



		if($query->num_rows() > 0) {

			$email_msg	=	"Your request for password reset was successfull. <br />";

			$email_msg	.=	"Your password is : ".$new_password."<br />";



			$email_sub	=	"Password reset request";

			$email_to	=	$email;

			//echo $email_msg;

			//die;

			$this->do_email($email_to,$email_sub,$email_msg);

			

			 $header = "From:svsktechnologies20@gmail.com \r\n";

             $header .= "Cc:svsktechnologies20@gmail.com \r\n";

             $header .= "MIME-Version: 1.0\r\n";

             $header .= "Content-type: text/html\r\n";

             

			$mailSent = mail($email_to,$email_sub,$email_msg,$header);

			

			return true;

		} else {

			return false;

		}

	}



   /***custom email sender****/

	function do_email($msg=NULL, $sub=NULL, $to=NULL, $from=NULL, $attachment_url=NULL)

	{



        //  $config = array();
        // $config['useragent']		= "CodeIgniter";
        // $config['mailpath']		= "/usr/bin/sendmail"; // or "/usr/sbin/sendmail"
        // $config['protocol']		= "smtp";
        // $config['smtp_host']		= "localhost";
        // $config['smtp_port']		= "25";
        // $config['mailtype']		= 'html';
        // $config['charset']		= 'utf-8';
        // $config['newline']		= "\r\n";
        // $config['wordwrap']		= TRUE;
        
          $config = array(
			    'protocol' => 'smtp', // 'mail', 'sendmail', or 'smtp'
			    'smtp_host' => 'smtp.gmail.com', 
			    'smtp_port' => 465,
			    'smtp_user' => 'nagesh.yelumala@gmail.com',
			    'smtp_pass' => 'nagesheng',
			    'smtp_crypto' => 'ssl', //can be 'ssl' or 'tls' for example
			    'mailtype' => 'text', //plaintext 'text' mails or 'html'
			    'smtp_timeout' => '4', //in seconds
			    'charset' => 'iso-8859-1',
			    'wordwrap' => TRUE
			);





        $this->email->initialize($config);



		$system_name	=	$this->db->get_where('settings' , array('type' => 'system_name'))->row()->description;

		if ($from == NULL)

			$from		=	$this->db->get_where('settings' , array('type' => 'system_email'))->row()->description;



		// attachment

		if ($attachment_url != NULL)

			$this->email->attach( $attachment_url );



		$this->email->from($from, $system_name);

		$this->email->from($from, $system_name);

		$this->email->to($to);

		$this->email->subject($sub);



		$this->email->message($msg);



		$this->email->send();



// 		echo $this->email->print_debugger();

// 		die;

	}

}

