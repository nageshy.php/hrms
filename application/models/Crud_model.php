<?php
/*

 *  @author     : Y Nagesh,SVSK TEAM

 *  date        : 10 july, 2020

 *  Human Resource Management System
    Nagesh.y@svsktechnologies.com
 */


if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Crud_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function clear_cache() {
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    function add_complaint($user_code) {
        $data['user_id']    =   $this->db->get_where('user', array('user_code' => $user_code))->row()->user_id;
        $data['title']  =   $this->input->post('title');
        $data['summary']  =   $this->input->post('summary');
        $data['timestamp']  =   strtotime($this->input->post('timestamp'));
        $this->db->insert('complaints', $data);
    }

    function edit_complaint($complaints_id) {
        $data['title']  =   $this->input->post('title');
        $data['summary']  =   $this->input->post('summary');
        $data['timestamp']  =   strtotime($this->input->post('timestamp'));

        $this->db->where('complaints_id', $complaints_id);
        $this->db->update('complaints', $data);

        $user_id = $this->db->get_where('complaints', array('complaints_id' => $complaints_id))->row()->user_id;
        $code = $this->db->get_where('user', array('user_id' => $user_id))->row()->user_code;

        return $code;
    }

    function delete_complaint($complaints_id) {

        $user_id = $this->db->get_where('complaints', array('complaints_id' => $complaints_id))->row()->user_id;
        $code = $this->db->get_where('user', array('user_id' => $user_id))->row()->user_code;

        $this->db->where('complaints_id', $complaints_id);
        $this->db->delete('complaints');

        return $code;
    }
    
    

    function curl_request($code = '') {

    //   $purchase_code = $code;
    //   $ch = curl_init();
  		// curl_setopt($ch, CURLOPT_URL, 'https://marketplace.envato.com/api/edge/Creativeitem/3q961vxab5xh5cys82lip6wchxgarrj3/verify-purchase:' . $purchase_code . '.json');
  		// curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (compatible; Envato Marketplace API Wrapper PHP)');
  		// curl_setopt($ch, CURLOPT_USERAGENT, 'API');
  		// curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
  		// curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  		// $result_from_json = curl_exec($ch);
  		// curl_close($ch);
  		// $result = json_decode($result_from_json, true);

  		// if (count($result['verify-purchase']) > 0) {
  		// 	return true;
  		// } else {
  		// 	return true;
    //   	}

  	}

    //CREATE DEPARTMENT
    function create_department() {
        $department_code            = substr(md5(rand(100000000, 20000000000)), 0, 15);
        $data['department_code']    = $department_code;
        $data['name']               = $this->input->post('name');
        $this->db->insert('department',$data);
        $department_id              = $this->db->insert_id();
        $designation                = $this->input->post('designation');

        foreach ($designation as $designation):
            if($designation != ""):
            $data2['department_id'] = $department_id;
            $data2['name']          = $designation;
            $this->db->insert('designation',$data2);
        endif;
        endforeach;
    }

    //EDIT DEPARTMENT//
    function edit_department($department_code = '') {
        $department_id = $this->db->get_where('department', array('department_code' => $department_code))->row()->department_id;

        $data['name'] = $this->input->post('name');

        $this->db->where('department_id', $department_id);
        $this->db->update('department', $data);

        // UPDATE EXISTING DESIGNATIONS
        $designations = $this->db->get_where('designation', array('department_id' => $department_id))->result_array();
        foreach ($designations as $row):
           $data2['name'] = $this->input->post('designation_' . $row['designation_id']);
           $this->db->where('designation_id',  $row['designation_id']);
           $this->db->update('designation', $data2);
        endforeach;

        // CREATE NEW DESIGNATIONS
        $designations = $this->input->post('designation');

        foreach($designations as $designation)
            if($designation != ""):
                $data3['department_id'] = $department_id;
                $data3['name']          = $designation;
                $this->db->insert('designation', $data3);
            endif;
    }

    //DELETE DEPARTMENT
    function delete_department($department_code = '') {
        $department_id = $this->db->get_where('department',array('department_code'=>$department_code))->row()->department_id;
        $this->db->where('department_id',$department_id);
        $this->db->delete('designation');
        $this->db->where('department_id',$department_id);
        $this->db->delete('department');
    }
    //CREATE EMPLOYEE//

    function create_employee() 
    {
        $data2['name']                  = $this->input->post('name');
        $data2['user_code']             = $this->input->post('user_code');
        $data2['date_of_birth']         = strtotime($this->input->post('date_of_birth'));
        $data2['gender']                = $this->input->post('gender');
        $data2['phone']                 = $this->input->post('phone');
        $data2['local_address']         = $this->input->post('local_address');
        $data2['permanent_address']     = $this->input->post('permanent_address');
        $data2['nationality']           = $this->input->post('nationality');
        $data2['email']                 = $this->input->post('email');
        $data2['password']              = sha1($this->input->post('password'));
        $data2['joining_salary']        = $this->input->post('joining_salary');
        $data2['reporting_to']        = $this->input->post('reporting_to');
        $data2['role']        = $this->input->post('role');
        $data2['status']                = 1;
        $data2['type']                  = 2;
        $data2['document_id']           = 0;
        
        $this->db->insert('user',$data2);

        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/user_image/' . $this->input->post('user_code') . '.jpg');
        $this->email_model->account_opening_email($this->input->post('email'), $this->input->post('password'));
       return true;
    }
    // function create_employee() {
    //            $document_id = '';
    //             //bank
    //             $data['name']                   = $this->input->post('bank_name');
    //             $data['branch']                 = $this->input->post('branch');
    //             $data['account_holder_name']    = $this->input->post('account_holder_name');
    //             $data['account_number']         = $this->input->post('account_number');
    //             $this->db->insert('bank',$data);
    //             $bank_id = $this->db->insert_id();

    //             //document
    //             if($_FILES['resume']['name'] != ''||$_FILES['offer_letter']['name'] != ''||$_FILES['joining_letter']['name'] != ''||$_FILES['contract_agreement']['name'] != ''||$_FILES['others']['name'] != ''){
    //                 if($_FILES['resume']['name'] != '')
    //                     $data3['resume']            = $this->input->post('user_code') . '_' . $_FILES['resume']['name'];
    //                 if($_FILES['offer_letter']['name'] != '')
    //                     $data3['offer_letter']            = $this->input->post('user_code') . '_' . $_FILES['offer_letter']['name'];
    //                 if($_FILES['joining_letter']['name'] != '')
    //                     $data3['joining_letter']            = $this->input->post('user_code') . '_' . $_FILES['joining_letter']['name'];
    //                 if($_FILES['contract_agreement']['name'] != '')
    //                     $data3['contract_agreement']            = $this->input->post('user_code') . '_' . $_FILES['contract_agreement']['name'];
    //                 if($_FILES['others']['name'] != '')
    //                     $data3['others']            = $this->input->post('user_code') . '_' . $_FILES['others']['name'];

    //                 $this->db->insert('document',$data3);
    //                 $document_id = $this->db->insert_id();

    //                 if($_FILES['resume']['name'] != '')
    //                     move_uploaded_file($_FILES['resume']['tmp_name'], 'uploads/document/resume/' . $data3['resume']);
    //                 if($_FILES['offer_letter']['name'] != '')
    //                     move_uploaded_file($_FILES['offer_letter']['tmp_name'], 'uploads/document/offer_letter/' . $data3['offer_letter']);
    //                 if($_FILES['joining_letter']['name'] != '')
    //                     move_uploaded_file($_FILES['joining_letter']['tmp_name'], 'uploads/document/joining_letter/' . $data3['joining_letter']);
    //                 if($_FILES['contract_agreement']['name'] != '')
    //                     move_uploaded_file($_FILES['contract_agreement']['tmp_name'], 'uploads/document/contract_agreement/' . $data3['contract_agreement']);
    //                 if($_FILES['others']['name'] != '')
    //                     move_uploaded_file($_FILES['others']['tmp_name'], 'uploads/document/others/' . $data3['others']);
    //             }

    //             //user
    //             $data2['name']                  = $this->input->post('name');
    //             $data2['father_name']           = $this->input->post('father_name');
    //             $data2['date_of_birth']         = strtotime($this->input->post('date_of_birth'));
    //             $data2['gender']                = $this->input->post('gender');
    //             $data2['phone']                 = $this->input->post('phone');
    //             $data2['local_address']         = $this->input->post('local_address');
    //             $data2['permanent_address']     = $this->input->post('permanent_address');
    //             $data2['nationality']           = $this->input->post('nationality');
    //             $data2['martial_status']        = $this->input->post('martial_status');
    //             $data2['email']                 = $this->input->post('email');
    //             $data2['password']              = sha1($this->input->post('password'));
    //             $data2['user_code']             = $this->input->post('user_code');
    //             $data2['department_id']         = $this->input->post('department_id');
    //             $data2['designation_id']        = $this->input->post('designation_id');
    //             $data2['date_of_joining']       = strtotime($this->input->post('date_of_joining'));
    //             $data2['joining_salary']        = $this->input->post('joining_salary');
    //             $data2['date_of_leaving']       = strtotime($this->input->post('date_of_leaving'));
    //             $data2['status']                = $this->input->post('status');
    //             $data2['type']                  = 2;
    //             $data2['bank_id']               = $bank_id;
    //             if($document_id != '')
    //             {
    //             $data2['document_id']           = $document_id;
    //             }
    //             else
    //             {
    //                 $data2['document_id']       =0;
    //             }
    //             $this->db->insert('user',$data2);
    //             $user_id = $this->db->insert_id();

    //             // job history
    //             $company_names = $this->input->post('company_name');
    //             if (count($company_names > 0)) {

    //                 for ($i=0; $i < count($company_names); $i++) {
    //                     $job_history_data['user_id']        =   $user_id;
    //                     $job_history_data['company_name']   =   $company_names[$i];
    //                     $job_history_data['department']     =   $this->input->post('department')[$i];
    //                     $job_history_data['designation']    =   $this->input->post('designation')[$i];
    //                     $job_history_data['timestamp_from'] =   strtotime($this->input->post('timestamp_from')[$i]);
    //                     $job_history_data['timestamp_to']   =   strtotime($this->input->post('timestamp_to')[$i]);

    //                     $this->db->insert('job_history', $job_history_data);
    //                 }
    //             }


    //             move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/user_image/' . $this->input->post('user_code') . '.jpg');
    //             $this->email_model->account_opening_email($this->input->post('email'), $this->input->post('password'));
    //        return true;
    // }
    //EDIT EMPLOYEE
    function edit_employee($user_code = '')
    {
        //bank
        if($this->input->post('bank_name') != '' || $this->input->post('branch') != '' || $this->input->post('account_holder_name') != '' || $this->input->post('account_number') != '')
        {
            $bank_id = $this->db->get_where('user',array('user_code'=>$user_code))->row()->bank_id;
            $data['name']                   = $this->input->post('bank_name');
            $data['branch']                 = $this->input->post('branch');
            $data['account_holder_name']    = $this->input->post('account_holder_name');
            $data['account_number']         = $this->input->post('account_number');
            if ($bank_id == null) 
            {
               $this->db->insert('bank',$data);
               $bank_id = $this->db->insert_id();
               $data2['bank_id'] = $bank_id;
            }
            else
            {
                $this->db->where('bank_id',$bank_id);
                $this->db->update('bank',$data);
            } 
        }
        
       
        

        //document
        if($_FILES['resume']['name'] != '' || $_FILES['offer_letter']['name'] != '' || $_FILES['joining_letter']['name'] != '' || $_FILES['contract_agreement']['name'] != '' || $_FILES['others']['name'] != '') {

            if($_FILES['resume']['name'] != '')
                $data3['resume'] = $user_code . '_' . $_FILES['resume']['name'];
            if($_FILES['offer_letter']['name'] != '')
                $data3['offer_letter'] = $user_code . '_' . $_FILES['offer_letter']['name'];
            if($_FILES['joining_letter']['name'] != '')
                $data3['joining_letter'] = $user_code . '_' . $_FILES['joining_letter']['name'];
            if($_FILES['contract_agreement']['name'] != '')
                $data3['contract_agreement'] = $user_code . '_' . $_FILES['contract_agreement']['name'];
            if($_FILES['others']['name'] != '')
                $data3['others'] = $user_code . '_' . $_FILES['others']['name'];

            $document_id = $this->db->get_where('user', array('user_code' => $user_code))->row()->document_id;

            if($document_id == 0) {
                $this->db->insert('document', $data3);
                $document_id = $this->db->insert_id();
            } else
                $this->db->update('document', $data3, array('document_id' => $document_id));


            if($_FILES['resume']['name'] != '')
                move_uploaded_file($_FILES['resume']['tmp_name'], 'uploads/document/resume/' . $data3['resume']);
            if($_FILES['offer_letter']['name'] != '')
                move_uploaded_file($_FILES['offer_letter']['tmp_name'], 'uploads/document/offer_letter/' . $data3['offer_letter']);
            if($_FILES['joining_letter']['name'] != '')
                move_uploaded_file($_FILES['joining_letter']['tmp_name'], 'uploads/document/joining_letter/' . $data3['joining_letter']);
            if($_FILES['contract_agreement']['name'] != '')
                move_uploaded_file($_FILES['contract_agreement']['tmp_name'], 'uploads/document/contract_agreement/' . $data3['contract_agreement']);
            if($_FILES['others']['name'] != '')
                move_uploaded_file($_FILES['others']['tmp_name'], 'uploads/document/others/' . $data3['others']);

            $data2['document_id'] = $document_id;
        }

        //user
        $data2['name']                  = $this->input->post('name');
        $data2['father_name']           = $this->input->post('father_name');
        $data2['date_of_birth']         = strtotime($this->input->post('date_of_birth'));
        $data2['gender']                = $this->input->post('gender');
        $data2['phone']                 = $this->input->post('phone');
        $data2['local_address']         = $this->input->post('local_address');
        $data2['permanent_address']     = $this->input->post('permanent_address');
        $data2['nationality']           = $this->input->post('nationality');
        $data2['martial_status']        = $this->input->post('martial_status');
        $data2['email']                 = $this->input->post('email');
        $data2['department_id']         = $this->input->post('department_id');
        $data2['designation_id']        = $this->input->post('designation_id');
        if($this->input->post('date_of_joining') != null)
        {
           $data2['date_of_joining']       = strtotime($this->input->post('date_of_joining')); 
        }
        $data2['joining_salary']        = $this->input->post('joining_salary');
        if ($this->input->post('date_of_leaving')!= null) 
        {
           $data2['date_of_leaving']       = strtotime($this->input->post('date_of_leaving'));
        }
        if ($this->input->post('status')!=  null) {
           $data2['status']                = $this->input->post('status');
        }
        

        $this->db->where('user_code',$user_code);
        $this->db->update('user',$data2);
        move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/user_image/' . $user_code . '.jpg');

        return true;
    }
    
     //ACTIVATE EMPLOYEE
    function employee_manage($user_code) {
          $data['type']     = 2;
        $this->db->where('user_code' , $user_code);
        $this->db->update('user',$data);
        return true;
    }
    
     //INACTIVE EMPLOYEE
    function employee_inactive($user_code) {
          $data['type']     = 0;
        $this->db->where('user_code' , $user_code);
        $this->db->update('user',$data);
        return true;
    }

    //DELETE EMPLOYEE
    function delete_employee($user_code) {
        $user = $this->db->get_where('user',array('user_code'=>$user_code));
        $bank_id = $user->row()->bank_id;
        $this->db->where('bank_id',$bank_id);
        $this->db->delete('bank');
        $document_id = $user->row()->document_id;
        $document = $this->db->get_where('document',array('document_id'=>$document_id));

        $this->db->where('document_id',$document_id);
        $this->db->delete('document');
        if (file_exists('uploads/user_image/' . $user_code . '.jpg'))
        unlink('uploads/user_image/'.$user_code.'.jpg');
        $this->db->where('user_code',$user_code);
        $this->db->delete('user');
        return true;
    }
    ////////IMAGE URL//////////
    function get_image_url($type = '', $code = '') {
        if (file_exists('uploads/' . $type . '_image/' . $code . '.jpg'))
            $image_url = base_url() . 'uploads/' . $type . '_image/' . $code . '.jpg';
        else
            $image_url = base_url() . 'uploads/user.jpg';

        return $image_url;
    }

    function change_password($user_id) {
        $type                       = $this->session->userdata('login_type');
        $old_password               = $this->input->post('old_password');
        $data = $this->db->get_where($type, array($type.'_id' => $user_id))->result_array();
        foreach ($data as $row) {
            if (sha1($old_password) == $row['password']) {
                $newpassword        = sha1($this->input->post('new_password'));
                $confirmpassword    = sha1($this->input->post('confirm_password'));
                if ($newpassword == $confirmpassword) {
                    $data = array("password" => $newpassword);
                    $this->db->where($type.'_id', $user_id);
                    $this->db->update($type, $data);
                    return true;
                }
            }
            return false;
        }
    }
//verifiy account
    function verify_account($student_id) {
        $data['status']     = 1;
        $this->db->where('student_id' , $student_id);
        $this->db->update('student',$data);
    }
    
    // Project
    function create_project()
    {
        $data['project_code'] = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['project_name']       = $this->input->post('project_name');
        $data['client']       = $this->input->post('client');
        $data['priority']     = $this->input->post('priority');
        $data['project_leader']    = $this->input->post('project_leader');
        //$data['team']    = $this->input->post('team');
        $data['start_date']       = strtotime($this->input->post('start_date'));
        $data['end_date']       = strtotime($this->input->post('end_date'));
        //$data['upload_document'] ='uploads/user_image/' . $this->input->post('user_code') . '.jpg';
        
        if($_FILES['upload_document']['name'] != '')
                $data1 = $data['project_code'] . '_' . $_FILES['upload_document']['name'];
        
        move_uploaded_file($_FILES['upload_document']['tmp_name'], 'uploads/project_docs/' .$data1);
        if($_FILES['upload_document']['name'] == '')
        {
            $data['document'] = '';
        }
        else
        {
            $data['document'] = 'uploads/project_docs/' .$data1;
        }
        $data['project_status'] = "On Progress";
        $data['created_at'] =  time();
        $data['status'] =  1;
        
        
        $team    = $this->input->post('team');
            $data11 = array();
        foreach($team as $key => $value)
        {
            $data11[$key]['team']=$value;
           //echo "<br/>";
           
           $datat['status'] = 1;
            $datat['created_at'] = time();
           $datat['project_code'] = $data['project_code'];
            $datat['team'] = $value;
            $this->db->insert('project_team',$datat);
        }
        
        $this->db->insert('projects',$data);
    }
    
    function update_project($id = '')
    {
        $data['project_code'] = $this->input->post('project_code');
        $data['project_name']       = $this->input->post('project_name');
        $data['client']       = $this->input->post('client');
        $data['priority']     = $this->input->post('priority');
        $data['description']     = $this->input->post('description');
        $data['project_leader']    = $this->input->post('project_leader');
        //$data['team']    = $this->input->post('team');
        $data['start_date']       = strtotime($this->input->post('start_date'));
        $data['end_date']       = strtotime($this->input->post('end_date'));
        //$data['upload_document'] ='uploads/user_image/' . $this->input->post('user_code') . '.jpg';
        if($_FILES['upload_document']['name'] == '')
        {
           $data['document'] = $this->input->post('upload_document1');
        }
        else
        {
             if($_FILES['upload_document']['name'] != '')
                    $data1 = $data['project_code'] . '_' . $_FILES['upload_document']['name'];
            
            move_uploaded_file($_FILES['upload_document']['tmp_name'], 'uploads/project_docs/' .$data1);
        
            $data['document'] = 'uploads/project_docs/' .$data1;
        }
        $data['project_status'] = "On Progress";
        $data['updated_at'] =  time();
        $data['status'] =  1;
        
        $this->db->where('project_code', $data['project_code']);
        $this->db->delete('project_team');
        
        $team    = $this->input->post('team');
            $data11 = array();
        foreach($team as $key => $value)
        {
            $data11[$key]['team']=$value;
           //echo "<br/>";
           
            $datat['status'] = 1;
            $datat['created_at'] = time();
           $datat['project_code'] = $data['project_code'];
            $datat['team'] = $value;
            
            $this->db->insert('project_team',$datat);
            // $this->db->update('project_team', $datat, array('project_code' => $projectCode));
        }
        $this->db->update('projects', $data, array('id' => $id));
    }
    
    function delete_project($id = '')
    {
        $id = $this->uri->segment(4);
        $this->db->where('id', $id);
        $this->db->delete('projects');
    }

    // AWARD
    function create_award()
    {
        $data['award_code'] = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['name']       = $this->input->post('name');
        $data['gift']       = $this->input->post('gift');
        $data['amount']     = $this->input->post('amount');
        $data['user_id']    = $this->input->post('user_id');
        $data['date']       = strtotime($this->input->post('date'));

        $this->db->insert('award',$data);
    }

    function update_award($award_id = '')
    {
        $data['name']       = $this->input->post('name');
        $data['gift']       = $this->input->post('gift');
        $data['amount']     = $this->input->post('amount');
        $data['user_id']    = $this->input->post('user_id');
        $data['date']       = strtotime($this->input->post('date'));

        $this->db->update('award', $data, array('award_id' => $award_id));
    }

    function delete_award($award_id = '')
    {
        $this->db->where('award_id', $award_id);
        $this->db->delete('award');
    }

    // EXPENSE
    function create_expense()
    {
        $data['expense_code'] = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['title']          = $this->input->post('title');
        $data['description']    = $this->input->post('description');
        $data['amount']         = $this->input->post('amount');
        $data['bill']         = $this->input->post('bill');
        $data['receive_from']         = $this->input->post('receive_from');
        $data['mode_of_amount']         = $this->input->post('mode_of_amount');
        //$data['balance_amount']         = $this->input->post('amount');
        $data['date']           = strtotime($this->input->post('date'));
        $data['status']  = 1;
        $data['created_at']  = time();
        
        $availableAmount = $this->db->get_where('expense')->result_array();
        foreach($availableAmount as $amount)
        {
            $aamaount = $amount['balance_amount'];
        }
        
        $data['balance_amount'] = $aamaount + $data['amount'];
         
        $this->db->insert('expense',$data);
    }

    function update_expense($expense_id = '')
    {
         $data['title']          = $this->input->post('title');
        $data['description']    = $this->input->post('description');
        $data['amount']         = $this->input->post('amount');
        $data['bill']         = $this->input->post('bill');
        $data['receive_from']         = $this->input->post('receive_from');
        $data['mode_of_amount']         = $this->input->post('mode_of_amount');
        //$data['date']           = strtotime($this->input->post('date'));
        $data['status']  = 1;
        $data['updated_at']  = time();

        $this->db->update('expense', $data, array('expense_id' => $expense_id));
    }
    
    function used_expense($expense_code = '')
    {
        $data['expense_code']          = $this->input->post('expense_code');
         $data['title']          = $this->input->post('title');
        $data['description']    = $this->input->post('description');
        $data['amount_used']         = $this->input->post('amount_used');
        $data['balance_amount']         = $this->input->post('balance_amount');
        $data['bill']         = $this->input->post('bill');
        $data['receive_from']         = $this->input->post('receive_from');
        $data['mode_of_amount']         = $this->input->post('mode_of_amount');
        //$data['date']           = strtotime($this->input->post('date'));
        $data['status']  = 3;
        $data['used_date']  = time();
        $data['created_at']  = time();
        
        $data['balance_amount'] = $data['balance_amount'] - $data['amount_used'];

        //$this->db->update('expense', $data, array('expense_id' => $expense_id));
        $this->db->insert('expense',$data);
    }

    function delete_expense($expense_id = '')
    {
        $this->db->where('expense_id', $expense_id);
        $this->db->delete('expense');
    }

    // NOTICEBOARD
    function create_noticeboard()
    {
        $data['noticeboard_code'] = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['title']          = $this->input->post('title');
        $data['description']    = $this->input->post('description');
        $data['status']         = $this->input->post('status');
        $data['date']           = strtotime($this->input->post('start_date'));
        $data['end_date']           = strtotime($this->input->post('end_date'));
        
        $this->db->insert('noticeboard',$data);
        
        date_default_timezone_set("Asia/Kolkata");
        $title = $data['title'] ;
        $desc = $data['description'];
        $startDate1 = date('yy-m-d',$data['date']) ;
        $endDate1 = date('yy-m-d',$data['end_date']);
        //die;
        
        
             $subject = "Subject : $title";
             
             //$message .= "<h5>Dear All</h5>";
             //$message .= "<p></p>";
              //$message .= "<p><b>Start Date :</b> $startDate <br/> <b>End Date :</b> $endDate</p>";
              $message .= "
              
              <html><head></head><body><div class='rcmBody'><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
						
						<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
							
							<table align='center' bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='center' class='padding'>
											
											<div style='display: inline-block; width: 100%; max-width: 600px; vertical-align: top' class='main-width'>
												<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-inner' width='100%' style='max-width: 600px'><tbody><tr><td height='10' style='mso-line-height-rule: exactly; line-height: 10px; font-size: 0'>
																&nbsp;
															</td>
														</tr><tr><td align='right' class='MsoNormal' style='padding: 0 10px; color: #666666; font-family: Arial, Helvetica Neue, Helvetica, sans-serif; font-size: 12px; font-weight: 400; letter-spacing: 1px'>
																 
															</td>
														</tr><tr><td height='10' style='mso-line-height-rule: exactly; line-height: 10px; font-size: 0'>
																&nbsp;
															</td>
														</tr></tbody></table></div>
											
										</td>
									</tr></tbody></table></div>
						
					</td>
				</tr></tbody></table><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
						
						<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
							
							<table align='center' bgcolor='#f0f0f0' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='center' class='padding'>
											
											<div style='display: inline-block; width: 100%; max-width: 600px; vertical-align: top' class='main-width'>
												<table align='center' border='0' class='display-width-inner' cellpadding='0' cellspacing='0' width='100%' style='max-width: 600px'><tbody><tr><td height='15' class='height30' style='mso-line-height-rule: exactly; line-height: 15px; font-size: 0'>
														</td>
													</tr><tr><td align='center' style='width: 100%; max-width: 100%; font-size: 0'>
															
															<div style='display: inline-block; max-width: 150px; width: 100%; vertical-align: top' class='div-width'>
																
																<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-child' width='100%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 100%'><tbody><tr><td align='center'>
																			<table align='center' border='0' cellpadding='0' cellspacing='0' style='width: auto !important'><tbody><tr><td align='center' style='color: #333333'>
																						 <a style='color: #333333; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'> <img src='".base_url()."/uploads/logo.png' alt='150x50x1' width='150' height='50' style='margin: 0; border: 0; padding: 0; display: block'></a>
																					</td>
																				</tr></tbody></table></td>
																	</tr></tbody></table></div>
															
															<div style='display: inline-block; width: 100%; max-width: 440px; vertical-align: top' class='div-width'>
																<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-child' width='100%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 100%'><tbody><tr><td align='center' width='100%' class='saf-table' style='font-size: 0; border-collapse: collapse; border-spacing: 0'>
																			
																			<div style='display: inline-block; width: 100%; max-width: 310px; vertical-align: top' class='div-width'>
																				<table align='left' border='0' cellpadding='0' cellspacing='0' class='display-width-child' width='100%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 310px'><tbody><tr><td width='100%' style='font-size: 0'>
																							&nbsp;
																						</td>
																					</tr></tbody></table></div>
																			
																			<div style='display: inline-block; width: 100%; max-width: 120px; vertical-align: top'>
																				<table align='right' border='0' cellpadding='0' cellspacing='0' class='display-width-child' width='100%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 100%'><tbody><tr><td align='center' style='font-size: 0'>
																							
																							<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-child' width='100%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; max-width: 100%'><tbody><tr><td align='center'>
																										<table align='center' border='0' width='100%' cellpadding='0' cellspacing='0'><tbody><tr><td height='9' class='height20' style='mso-line-height-rule: exactly; line-height: 9px; font-size: 0'>
																													&nbsp;
																												</td>
																											</tr><tr><td align='left' valign='middle' width='32'>
																													 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'><img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x1.png' alt='32x32x1' width='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																												</td>
																												<td width='10'>
																													&nbsp;
																												</td>
																												<td align='left' valign='middle' width='32'>
																													 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'><img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x2.png' alt='32x32x2' width='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																												</td>
																												<td width='10'>
																													&nbsp;
																												</td>
																												<td align='left' valign='middle' width='32'>
																													 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'><img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x3.png' alt='32x32x3' width='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																												</td>
																											</tr><tr><td height='9' class='hide-height' style='mso-line-height-rule: exactly; line-height: 9px; font-size: 0'>
																													&nbsp;
																												</td>
																											</tr></tbody></table></td>
																								</tr></tbody></table></td>
																					</tr></tbody></table></div>
																			
																		</td>
																	</tr></tbody></table></div>
															
														</td>
													</tr><tr><td height='15' class='height30' style='mso-line-height-rule: exactly; line-height: 15px; font-size: 0'>
															&nbsp;
														</td>
													</tr></tbody></table></div>
											
										</td>
									</tr></tbody></table></div>
						
					</td>
				</tr></tbody></table><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
					
					<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
						
						<table align='center' bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='left' width='680'>
									 <img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/680x200x2.jpg' alt='680x200x2' width='680' height='200' style='margin: 0; border: 0; width: 100%; max-width: 100%; display: block; height: auto'></td>
							</tr></tbody></table></div>
					
				</td>
			</tr></tbody></table><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
						
						<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
							
							<table align='center' bgcolor='#96000B' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='center' class='padding'>
											
											<div style='display: inline-block; width: 100%; max-width: 600px; vertical-align: top' class='main-width'>
												<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-inner' width='100%' style='max-width: 600px'><tbody><tr><td height='60' style='mso-line-height-rule: exactly; line-height: 60px; font-size: 0'>
																&nbsp;
															</td>
														</tr><tr><td align='center' class='MsoNormal' style='color: #ffffff; font-family: 'Segoe UI', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-weight: 600; font-size: 24px; line-height: 34px; letter-spacing: 1px'>
																 $title
															</td>
														</tr><tr><td height='20' style='mso-line-height-rule: exactly; line-height: 20px; font-size: 0'>
																&nbsp;
															</td>
														</tr><tr><td align='center'>
																<table align='center' border='0' cellpadding='0' cellspacing='0' width='90%' style='border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 90%; max-width: 90%'><tbody><tr><td align='center' class='MsoNormal' style='color: #ffffff; font-family: 'Segoe UI', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 14px; line-height: 24px; font-weight: 400'>
																			 Dear All, <br/>$desc
																			 </td>
																	</tr><tr><td height='10' style='mso-line-height-rule: exactly; line-height: 10px; font-size: 0'>
																			&nbsp;
																			Start Date : $startDate1 <br/>
																			End Date : $endDate1
																		</td>
																	</tr><tr>
																	</tr><tr><td height='20' style='mso-line-height-rule: exactly; line-height: 20px; font-size: 0'>
																			&nbsp;
																		</td>
																	</tr><tr><td align='right' class='MsoNormal txt-center' style='color: #ffffff; font-family: 'Segoe UI', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 14px; line-height: 24px; font-weight: 600; letter-spacing: 1px'>
																			 Best Wishes,
																		</td>
																	</tr><tr><td align='right' class='MsoNormal txt-center' style='color: #ffffff; font-family: 'Segoe UI', Helvetica Neue, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 13px; line-height: 23px; font-weight: 600; letter-spacing: 1px'>
																			 <img src='".base_url()."/uploads/logo.png' alt='150x50x1' width='150' height='50' style='margin: 0; border: 0; padding: 0; display: block'>
																		</td>
																	</tr></tbody></table></td>
														</tr><tr><td height='60' style='mso-line-height-rule: exactly; line-height: 60px; font-size: 0'>
																&nbsp;
															</td>
														</tr></tbody></table></div>
											
										</td>
									</tr></tbody></table></div>
						
					</td>
				</tr></tbody></table><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
					
					<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
						
						<table align='center' bgcolor='#ffffff' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='left' width='680'>
									 <img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/680x200x3.jpg' alt='680x200x3' width='680' height='200' style='margin: 0; border: 0; width: 100%; max-width: 100%; display: block; height: auto'></td>
							</tr></tbody></table></div>
					
				</td>
			</tr></tbody></table><table align='center' bgcolor='#333333' border='0' cellpadding='0' cellspacing='0' width='100%'><tbody><tr><td align='center'>
						
						<div style='display: inline-block; width: 100%; max-width: 680px; vertical-align: top' class='width680'>
							
							<table align='center' bgcolor='#f0f0f0' border='0' cellpadding='0' cellspacing='0' class='display-width' width='100%' style='max-width: 680px'><tbody><tr><td align='center' class='padding'>
										
										<div style='display: inline-block; width: 100%; max-width: 600px; vertical-align: top' class='main-width'>
											<table align='center' border='0' cellpadding='0' cellspacing='0' class='display-width-inner' width='100%' style='max-width: 600px'><tbody><tr><td height='60' style='line-height: 60px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td align='center'>
														<table align='center' border='0' cellspacing='0' cellpadding='0' style='width: auto !important'><tbody><tr><td align='left' valign='middle' width='32'>
																	 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'> <img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x1.png' alt='32x32x1' width='32' height='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																</td>
																<td width='20'>
																</td>
																<td align='left' valign='middle' width='32'>
																	 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'> <img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x2.png' alt='32x32x2' width='32' height='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																</td>
																<td width='20'>
																</td>
																<td align='left' valign='middle' width='32'>
																	 <a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'> <img src='http://www.stampready.net/dashboard/editor/user_uploads/zip_uploads/2018/12/07/RIx1Ovur4SeAVnMEb9NjZKcy/notification-4/images/32x32x3.png' alt='32x32x3' width='32' height='32' style='margin: 0; border: 0; padding: 0; display: block'></a>
																</td>
															</tr></tbody></table></td>
												</tr><tr><td height='15' style='line-height: 15px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td align='center' class='MsoNormal' style='color: #666666; font-family: Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; letter-spacing: 1px'>
														
													</td>
												</tr><tr><td height='10' style='line-height: 10px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td height='10' style='line-height: 10px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td align='center' class='MsoNormal' style='color: #666666; font-family: Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 14px; font-weight: 400; line-height: 24px; letter-spacing: 1px'>
														 +123456789
													</td>
												</tr><tr><td height='20' style='border-bottom: 1px solid #cccccc; line-height: 20px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td height='20' style='line-height: 20px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr><tr><td align='center' class='MsoNormal' style='color: #666666; font-family: Segoe UI, Arial, Verdana, Trebuchet MS, sans-serif; font-size: 12px; font-weight: 400; line-height: 22px; letter-spacing: 1px'>
														<span class='txt-copyright unsub-width'>©2020, All Rights Reserved </span> <span class='txt-copyright hide-bar'>|</span> <span class='txt-unsubscribe unsub-width'><a style='color: #666666; text-decoration: none' href='./#NOP' onclick='return false' rel='noreferrer'>Unsubscribe</a></span>
													</td>
												</tr><tr><td height='60' style='line-height: 60px; mso-line-height-rule: exactly; font-size: 0'>
														&nbsp;
													</td>
												</tr></tbody></table></div>
										
									</td>
								</tr></tbody></table></div>
						
					</td>
				</tr></tbody></table><img src='https://u3605716.ct.sendgrid.net/wf/open?upn=FVaZd65CRQbGtZXv6F-2BqnXHV6apTh01pczYIn6-2BGq76QCLAKwXg4Ahaez5Dig77uid-2FNN-2Flgri-2BDvQcihszcILQyBKa-2B6TEtOPOQCvqdO7jaJc4DFRXQ5-2BXVnItxGYOf99mw5CbFbExflcp904rCV4MWosp-2FK6MxYJZ1XCBOGvxNHdLyQ5p9MDbVKURqqgIASeRd3W6ZlPvhq55li8RfetfUnoceFLXz8YQPqWOdfq3cj4HJmSqIamPhpfkTbcV4' width='1' height='1' border='0' style='height: 1px !important; width: 1px !important; border-width: 0 !important; margin-top: 0 !important; margin-bottom: 0 !important; margin-right: 0 !important; margin-left: 0 !important; padding-top: 0 !important; padding-bottom: 0 !important; padding-right: 0 !important; padding-left: 0 !important'></div>
                    </body></html>
              
              
              ";
             
             $header = "From:nagesh.y@svsktechnologies.com \r\n";
             $header .= "Cc:test@svsktechnologies.com \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
        $emails = $this->db->get_where('user', array('status' => 1))->result_array();
            
         foreach($emails as $emailsArray)
        {
            $emails = $emailsArray['email'];
            $to = $emails;
        
            $retval = mail ($to,$subject,$message,$header);
        }
       
        
        
    }

    function update_noticeboard($noticeboard_id = '')
    {
        $data['title']          = $this->input->post('title');
        $data['description']    = $this->input->post('description');
        $data['status']         = $this->input->post('status');
        $data['date']           = strtotime($this->input->post('date'));

        $this->db->update('noticeboard', $data, array('noticeboard_id' => $noticeboard_id));
    }

    function delete_noticeboard($noticeboard_id = '')
    {
        $this->db->where('noticeboard_id', $noticeboard_id);
        $this->db->delete('noticeboard');
    }

    // LEAVE
    function create_leave()
    {
        $data['leave_code']     = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['leave_type']        = $this->input->post('leave_type');
        $data['user_id']        = $this->session->userdata('login_user_id');
        $data['start_date']     = strtotime($this->input->post('start_date'));
        $data['end_date']       = strtotime($this->input->post('end_date'));
        $data['reason']         = $this->input->post('reason');
        
        //echo $data['leave_type'];
        //die;
        
        $this->db->insert('leave',$data);
        
        $leaveCode = $data['leave_code'];
        $leaveType = $data['leave_type'];
        $userId = $data['user_id'];
        $startDate = date('yy-m-d',$data['start_date']);
        $endDate = date('yy-m-d',$data['end_date']);
        $reason = $data['reason'];
        
        if($endDate == " ")
        {
            $endDate = $startDate;
        }
        
        $emails = $this->db->get_where('emails',array('designation_id'=>1))->result_array();
        foreach($emails as $emailsend)
        {
            $to = $emailsend['email'];
        }
        
        $user = $this->db->get_where('user', array('user_id' => $userId))->row();
        $name = $user->name;
        
        $leave1 = $this->db->get_where('leave', array('leave_code' => $leaveCode))->row();
        $leaveid = $leave1->leave_id;
        
        $aemail = $this->db->get_where('emails', array('designation_id'=>2))->row();
        $hr = $aemail->email;
        
        $tlmail = $this->db->get_where('emails', array('designation_id'=>5))->row();
        $teamLeader = $tlmail->email;
        
        
         $fromemail = $this->db->get_where('emails', array('designation_id'=>0))->row();
        $fromemail = $fromemail->email;

        $apprlnk = "".base_url()."admin/leave/approve/$leaveid";
        $rjlnk = "".base_url()."admin/leave/decline/$leaveid";
        
        //$to = "nagesh.y@svsktechnologies.com";
         $subject = "Subject : Aplying For Leave - $name";
         if($to)
         {
             $message = "<b>Leave Code : $leaveCode </b><br/>";
             $message .= "<b>Leave Type : $leaveType </b>";
             $message .= "<h5>Dear Mr./Mrs. Sir/Madam</h5>";
             $message .= "<p>I am requesting you for a leave from : $startDate to : $endDate since $reason.<br/> </p>";
              $message .= "<p>I will be very greatful to you for considering my request.</p>";
              $message .= "<a href = $apprlnk>". "<button style = 'background-color: #18a22f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #18a22f;'>Confirm Leave</button></a> &nbsp; <a href = $rjlnk><button style = 'background-color: ##dc472f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #dc472f;'>Reject</button></a>";
              $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             //$message .= "<p>Reason :  $reason</p>";
             
             $header = "From:$fromemail \r\n";
             $header .= "Cc:$fromemail \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
             $retval = mail ($to,$subject,$message,$header);
         }
         
         if($hr)
         {
              $message = "<b>Leave Code : $leaveCode </b><br/>";
             $message .= "<b>Leave Type : $leaveType </b>";
             $message .= "<h5>Dear Mr./Mrs. Sir/Madam</h5>";
             $message .= "<p>I am requesting you for a leave from : $startDate to : $endDate since $reason.<br/> </p>";
              $message .= "<p>I will be very greatful to you for considering my request.</p>";
             // $message .= "<a href = $apprlnk>". "<button style = 'background-color: #18a22f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #18a22f;'>Confirm Leave</button></a> &nbsp; <a href = $rjlnk><button style = 'background-color: ##dc472f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #dc472f;'>Reject</button></a>";
              $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             //$message .= "<p>Reason :  $reason</p>";
             
             $header = "From:$fromemail \r\n";
             //$header .= "Cc:$fromemail \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             //echo $message;
             $retval = mail ($hr,$subject,$message,$header);
         }
         
         if($teamLeader)
         {
             $dept = $this->db->get_where('user', array('user_id' => $userId))->row();
             $deptId = $dept->department_id;
             
              $tlmail = $this->db->get_where('user', array('email' => $teamLeader))->row();
            $tlDeptId = $tlmail->department_id;
            $tlDeptId = 3;
             
           if($deptId = $tlDeptId)
           {
               
             $message = "<b>Leave Code : $leaveCode </b><br/>";
             $message .= "<b>Leave Type : $leaveType </b>";
             $message .= "<h5>Dear Mr./Mrs. Sir</h5>";
             $message .= "<p>I am requesting you for a leave from : $startDate to : $endDate since $reason.<br/> </p>";
              $message .= "<p>I will be very greatful to you for considering my request.</p>";
             // $message .= "<a href = $apprlnk>". "<button style = 'background-color: #18a22f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #18a22f;'>Confirm Leave</button></a> &nbsp; <a href = $rjlnk><button style = 'background-color: ##dc472f;color: #fff;padding: 5px;font-family: cursive;box-shadow: 1px 1px 13px 5px #ddd;border: 1px solid #dc472f;'>Reject</button></a>";
              $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             //$message .= "<p>Reason :  $reason</p>";
             
             $header = "From:$fromemail \r\n";
             //$header .= "Cc:$fromemail \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
             $retval = mail ($teamLeader,$subject,$message,$header);
           }
         }
        
        
    }
    
    // SUPPORT
    function create_support()
    {
        $data['token']     = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['user_id'] = $this->session->userdata('login_user_id');
        $data['request_to'] = $this->input->post('request_to');
        $data['request_type'] = $this->input->post('request_type');
        $data['subject'] = $this->input->post('subject');
        $data['description'] = $this->input->post('description');
        
        $data['created_at'] = time();
        //$data['status'] = 1;
        $userId = $data['user_id'];
        $user = $this->db->get_where('user', array('user_id' => $userId))->row();
        $name = $user->name;
        
        $res = $this->db->insert('support',$data);
        if($res)
        {
            $usr = $this->db->get_where('user',array('name'=>$data['request_to']))->row();
            $to = $usr->email;
            $subject = "Support Mail - ".$data['subject'];
            $message =  "<br/><b>Token :</b>" . $data['token'];
            $message .= "<br/><b>Request Type : </b>". $data['request_type']."<br/><b>Description :</b>". $data['description'];
            $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             $header = "From:info@svsktechnologies.com \r\n";
             $header .= "Cc:nagesh.y@svsktechnologies.com \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
             $retval = mail ($to,$subject,$message,$header);
            
           return true; 
        }
        else
        {
            return false;
        }
    }
    
     function support_edit($token = '')
    {
        $token  = $this->input->post('token');
        $data['user_id'] = $this->session->userdata('login_user_id');
        $data['request_to'] = $this->input->post('request_to');
        $data['request_type'] = $this->input->post('request_type');
        $data['subject'] = $this->input->post('subject');
        $data['description'] = $this->input->post('description');
        
        $data['updated_at'] = time();
        //$data['status'] = 1;
        $userId = $data['user_id'];
        $user = $this->db->get_where('user', array('user_id' => $userId))->row();
        $name = $user->name;
        
        $res = $this->db->update('support', $data, array('token' => $token));
        if($res)
        {
            $usr = $this->db->get_where('user',array('name'=>$data['request_to']))->row();
            $to = $usr->email;
            $subject = "Support Mail - ".$data['subject'];
            $message =  "<br/><b>Token :</b>" . $token;
            $message .= "<br/><b>Request Type : </b>". $data['request_type']."<br/><b>Description :</b>". $data['description'];
            $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             $header = "From:info@svsktechnologies.com \r\n";
             $header .= "Cc:nagesh.y@svsktechnologies.com \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
             $retval = mail ($to,$subject,$message,$header);
            
           return true; 
        }
        else
        {
            return false;
        }
    }
    
    
     function support_complete($token = '')
    {
        $token  = $this->input->post('token');
    
        $userId = $this->session->userdata('login_user_id');
        $data['report_status'] = $this->input->post('report_status');
        $data['report_description'] = $this->input->post('report_description');
        
        $data['report_date'] = time();
       
        if($data['report_status'] == "complete")
        {
            $data['report_status'] = 1;
            $status = "Your Support Request is Completed";
             $data['status'] = 1;
        }
        else if($data['report_status'] == "pending")
        {
            $data['report_status'] = 2;
             $status = "Your Support Request is Pending";
              $data['status'] = 0;
        }
        else if($data['report_status'] == "cancel")
        {
            $data['report_status'] = 3;
             $status = "Your Support Request is Cancelled";
              $data['status'] = 2;
        }
        
        //$userId = $data['user_id'];
        $user = $this->db->get_where('user', array('user_id' => $userId))->row();
        $name = $user->name;
        
        $userId = $this->db->get_where('support', array('token' => $token))->row();
        $uId = $userId->user_id;
        
        $userEmail = $this->db->get_where('user', array('user_id' => $uId))->row();
        $uEmail = $userEmail->email;
        
        $res = $this->db->update('support', $data, array('token' => $token));
        if($res)
        {
            $usr = $this->db->get_where('user',array('name'=>$data['request_to']))->row();
            $to = $usr->email;
            $subject = "Support Mail - ".$status;
            $message =  "<br/><b>Token :</b>" . $token;
            $message .= "<br/><b>Status : </b>". $status;
            $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
             $header = "From:info@svsktechnologies.com \r\n";
             $header .= "Cc:nagesh.y@svsktechnologies.com \r\n";
             $header .= "Bcc:$uEmail \r\n";
             $header .= "MIME-Version: 1.0\r\n";
             $header .= "Content-type: text/html\r\n";
             
             $retval = mail ($to,$subject,$message,$header);
            
           return true; 
        }
        else
        {
            return false;
        }
    }
    
    
    
    function delete_support($token = '')
    {
        $token = $this->uri->segment(4);
        $this->db->where('token', $token);
        $this->db->delete('support');
    }

    function update_leave($leave_id = '')
    {
        $data['start_date']     = strtotime($this->input->post('start_date'));
        $data['end_date']       = strtotime($this->input->post('end_date'));
        $data['reason']         = $this->input->post('reason');

        $this->db->update('leave', $data, array('leave_id' => $leave_id));
    }

    function delete_leave($leave_id = '')
    {
        $this->db->where('leave_id', $leave_id);
        $this->db->delete('leave');
    }
    
    //BREAK TIME
    function add_breaktime() {
        
        $data['user_id']        = $this->session->userdata('login_user_id');
        date_default_timezone_set("Asia/Kolkata"); 
        //$date = date('Y-m-d H:i:s',time());
       $data['breakin']  =  date('Y-m-d H:i:s',time());
       $data['date'] = date('Y-m-d',time());
       $data['breaktime_status'] = 1;
        $data['breaktime_token']     = substr(md5(rand(100000000, 20000000000)), 0, 7);
        
    //     date_default_timezone_set("Asia/Kolkata"); 
    //   //echo "success"; 
    //     $data['ip'] = getenv('REMOTE_ADDR');
    //     $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
    //     ;
    //      $data['token'] = city(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 10);
    //      $data['city'] = $geo["geoplugin_city"];
    //      $data['lattitude']=$geo["geoplugin_latitude"];
    //      $data['longitude']=$geo["geoplugin_longitude"];
    //      $data['region'] = $geo["geoplugin_regionName"];
        
        $this->session->set_userdata('breaktime_token' , $data['breaktime_token']);
        $this->db->insert('user_log', $data);
    }
    
    function update_breaktime() {
        
        $this->session->unset_userdata('breaktime_token');
        date_default_timezone_set("Asia/Kolkata"); 
        $data['breakout']  =   date('Y-m-d H:i:s',time());;
        $data['breaktime_status'] = 0;
        $breaktime_token     = $this->session->userdata('breaktime_token');
        $this->session->set_userdata('breaktime_token' , $data['breaktime_token']);
        
        $this->db->update('user_log', $data, array('breaktime_token' => $breaktime_token));
    }
    
    // Work Report
    function add_work_report()
    {
        $data['report_code']     = substr(md5(rand(100000000, 20000000000)), 0, 7);
        $data['project_code']        = $this->input->post('project_code');
        $data['project_module']        = $this->input->post('project_module');
        $data['url_type']        = $this->input->post('url_type');
        $data['work_types']        = $this->input->post('work_types');
        $data['work_date']        = $this->input->post('work_date');
        $data['duration']        = $this->input->post('duration');
        $data['start_time']        = $this->input->post('start_time');
        $data['end_time']        = $this->input->post('end_time');
        $data['details']        = $this->input->post('details');
        
        $data['created_at']        = time();
        $data['status']        = 1;
        
        $data['user_id']        = $this->session->userdata('login_user_id');
        
        //echo $data['leave_type'];
        //die;
        
        $this->db->insert('work_report',$data);
        
        $projectCode = $data['project_code'];
        $project = $this->db->get_where('projects',array('project_code'=>$projectCode))->row();
        $projectName = $project->project_name;
        $urltype = $data['url_type'];
        $worktype = $data['work_types'];
        $workdate = $data['work_date'];
        $duration = $data['duration'];
        
        $projectModule = $data['project_module'];
        $details = $data['details'];
        $userId = $data['user_id'];
        $startTime = $data['start_time'];
        $endTime = $data['end_time'];
       
        
        $emails = $this->db->get_where('emails',array('designation_id'=>1))->result_array();
        foreach($emails as $emailsend)
        {
            $to = $emailsend['email'];;
        }
        
        $fromMail = $this->db->get_where('emails',array('designation_id'=>0))->row();
        
        $frMail = $fromMail->email;
        
        
        $user = $this->db->get_where('user', array('user_id' => $userId))->row();
        $name = $user->name;
        
         $subject = "Subject : Work Report - $name";
         
         $message = "<b>Project Name : $projectName </b>";
         $message .= "<h5>Dear Mr./Mrs. Sir</h5>";
         $message .= "<p><b>URL Type :</b> $urltype <br/><b>Work Type :</b> $worktype <br/><b>Date :</b> $workdate <br/><b>Duration :</b> $duration .hrs <br/> <b>Start Time :</b> $startTime<br/><b>End Time :</b> $endTime</p>";
          $message .= "<p><b>Project Module:</b> $projectModule <br/><b> Details : $details</p>";
          
          $message .= "<p><br/><br/><br/>Yours Sincerely,<br/>$name. </p>";
         //$message .= "<p>Reason :  $reason</p>";
         
         $header = "From:$frMail \r\n";
         $header .= "Cc:$to \r\n";
         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";
         
         $retval = mail ($to,$subject,$message,$header);
        
        
    }
    
    function update_work_report()
    {
        $reportCode     = $this->input->post('report_code');
        //$data['project_code']        = $this->input->post('project_code');
        $data['project_module']        = $this->input->post('project_module');
        $data['url_type']        = $this->input->post('url_type');
        $data['work_types']        = $this->input->post('work_types');
        $data['work_date']        = $this->input->post('work_date');
        $data['duration']        = $this->input->post('duration');
        $data['start_time']        = $this->input->post('start_time');
        $data['end_time']        = $this->input->post('end_time');
        $data['details']        = $this->input->post('details');
        
        $data['created_at']        = time();
        $data['status']        = 1;
        
        $data['user_id']        = $this->session->userdata('login_user_id');
        
        
       $this->db->update('work_report', $data, array('report_code' => $reportCode));

    }
    
    function delete_work_report($id = '')
    {
         $id     =  $this->uri->segment(4);
        
        $this->db->where('id', $id);
        $this->db->delete('work_report');
    }
    
    

    // PRIVATE MESSAGING
    function send_new_private_message()
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));

        $reciever = $this->input->post('reciever');
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');

        //check if the thread between those 2 users exists, if not create new thread
        $num1 = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->num_rows();
        $num2 = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->num_rows();

        if ($num1 == 0 && $num2 == 0) {
            $message_thread_code = substr(md5(rand(100000000, 20000000000)), 0, 15);
            $data_message_thread['message_thread_code'] = $message_thread_code;
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $data_message_thread['status'] = 2;
            $this->db->insert('message_thread', $data_message_thread);
        }
        if ($num1 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $sender, 'reciever' => $reciever))->row()->message_thread_code;
        if ($num2 > 0)
            $message_thread_code = $this->db->get_where('message_thread', array('sender' => $reciever, 'reciever' => $sender))->row()->message_thread_code;


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['read_status'] = 2;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());

        return $message_thread_code;
    }

    function send_reply_message($message_thread_code)
    {
        $message = $this->input->post('message');
        $timestamp = strtotime(date("Y-m-d H:i:s"));
        $sender = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');


        $data_message['message_thread_code'] = $message_thread_code;
        $data_message['message'] = $message;
        $data_message['sender'] = $sender;
        $data_message['timestamp'] = $timestamp;
        $res = $this->db->insert('message', $data_message);

        // notify email to email reciever
        //$this->email_model->notify_email('new_message_notification', $this->db->insert_id());
       
         if ($this->session->userdata('login_type') == 'admin') {
           
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $data_message_thread['status'] = 2;
          
            $this->db->where('message_thread_code', $message_thread_code);
            $this->db->update('message', array('read_status' => 2));
            
            
        }
         if ($this->session->userdata('login_type') == 'employee') {
           
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $data_message_thread['status'] = 2;
          
            $this->db->where('message_thread_code', $message_thread_code);
            $this->db->update('message_thread', array('status' => 2));
            
            
        }
    }

    function mark_thread_messages_read($message_thread_code)
    {
        // mark read only the oponnent messages of this thread, not currently logged in user's sent messages
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $this->db->where('sender !=', $current_user);
        $this->db->where('message_thread_code', $message_thread_code);
        $res = $this->db->update('message', array('read_status' => 1));
        
        
        
         if ($this->session->userdata('login_type') == 'admin') {
           
            $data_message_thread['sender'] = $sender;
            $data_message_thread['reciever'] = $reciever;
            $data_message_thread['status'] = 2;
          
            $this->db->where('message_thread_code', $message_thread_code);
            $this->db->update('message_thread', array('status' => 1));
            
            
        }
    }

    function count_unread_message_of_thread($message_thread_code)
    {
        $unread_message_counter = 0;
        $current_user = $this->session->userdata('login_type') . '-' . $this->session->userdata('login_user_id');
        $messages = $this->db->get_where('message', array('message_thread_code' => $message_thread_code))->result_array();
        foreach ($messages as $row) {
            if ($row['sender'] != $current_user && $row['read_status'] == '2')
                $unread_message_counter++;
        }
        return $unread_message_counter;
    }

    // VACANCY
    function create_vacancy()
    {
        $data['department']             = $this->input->post('department');
        $data['qualification']             = $this->input->post('qualification');
        $data['name']                   = $this->input->post('name');
        $data['number_of_vacancies']    = $this->input->post('number_of_vacancies');
        $data['last_date']              = strtotime($this->input->post('last_date'));
        $data['last_joining_date']              = strtotime($this->input->post('last_joining_date'));
        
         $publishDate = date('dmy',$data['last_date']);
         $nov = $data['number_of_vacancies'];
        
        $myStr = $data['name'];
        $result = ucfirst(substr($myStr, 0, 3));
        $data['publishing_code']    = $result.$nov.$publishDate;
        
        $this->db->insert('vacancy',$data);
    }

    function update_vacancy($vacancy_id = '')
    {
        $data['department']             = $this->input->post('department');
        $data['qualification']             = $this->input->post('qualification');
        $data['name']                   = $this->input->post('name');
        $data['number_of_vacancies']    = $this->input->post('number_of_vacancies');
        $data['last_date']              = strtotime($this->input->post('last_date'));
        $data['last_joining_date']              = strtotime($this->input->post('last_joining_date'));
        
         $publishDate = date('dmy',$data['last_date']);
         $nov = $data['number_of_vacancies'];
        
        $myStr = $data['name'];
        $result = ucfirst(substr($myStr, 0, 3));
        $data['publishing_code']    = $result.$nov.$publishDate;

        $this->db->update('vacancy', $data, array('vacancy_id' => $vacancy_id));
    }

    function delete_vacancy($vacancy_id = '')
    {
        $this->db->where('vacancy_id', $vacancy_id);
        $this->db->delete('vacancy');
    }

    // APPLICATION
    function create_application()
    {
        $data['applicant_name'] = $this->input->post('applicant_name');
        $data['vacancy_id']     = $this->input->post('vacancy_id');
        $data['apply_date']     = strtotime($this->input->post('apply_date'));
        $data['status']         = $this->input->post('status');

        $this->db->insert('application', $data);
    }

    function update_application($application_id = '')
    {
        $old_vacancy_id = $this->db->get_where('application', array('application_id' => $application_id))->row()->vacancy_id;

        $data['applicant_name'] = $this->input->post('applicant_name');
        $data['vacancy_id']     = $this->input->post('vacancy_id');
        $data['apply_date']     = strtotime($this->input->post('apply_date'));
        $data['status']         = $this->input->post('status');

        $this->db->update('application', $data, array('application_id' => $application_id));
    }

    function delete_application($application_id = '')
    {
        $this->db->where('application_id', $application_id);
        $this->db->delete('application');
    }
}
